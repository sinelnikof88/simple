<div class="" style="width: 284.6px;">
    <div class="team-block team-v1">
        <div class="team-image">
            <img width="400" height="468" src="<?= $data->image ?>" class="attachment-winnex_height size-winnex_height wp-post-image" alt="">

            <div class="team-content">
                <div class="team-name">
                    <?= $data->name ?>   
                </div>
                <div class="team-job"><?= $data->position ?></div>
            </div>
        </div>
        <div class="team-content-bottom">
        </div>
    </div>
</div>