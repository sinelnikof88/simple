<div class="container">
    <div class="main-page-content row">
        <div class="content-page col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div id="wp-content" class="wp-content clearfix">
                <article id="post-257" class="post-257 post type-post status-publish format-gallery has-post-thumbnail hentry category-fashion post_format-post-format-gallery">
                    <div class="post-thumbnail">
                        <img width="1170" height="658" src="/site/resized/600x200/<?= $model->image ?>" class="attachment-full size-full wp-post-image"
                             alt="The best apps to your next trip"> 
                    </div>
                    <div class="entry-content">
                        <div class="content-inner">
                            <h1 class="entry-title"><?= $model->name ?></h1>
                            <div class="field-item even" style="min-height: 300px">
                                <?php
                                $this->widget('ext.wysibb.WysiBbParser', array(
                                    'text' => $model->text,
                                ));
                                ?>
                            </div>
                        </div>
                </article>

            </div>
        </div>
        <div class="sidebar wp-sidebar sidebar-right col-lg-3 col-md-3 col-xs-12 pull-right">
            <div class="sidebar-inner">
                <aside id="recent-posts-3" class="widget clearfix widget_recent_entries">
                    <h3 class="widget-title"><span>Последние страницы</span></h3> 
                    <ul>
                        <?php foreach (Pages::model()->order('id desc')->limit(6)->findAll() as $value): ?>
                            <li>
                                <a href="<?= @$value->link ?>"><?= @$value->name ?></a>
                            </li>
                        <?php endforeach; ?>

                    </ul>
                </aside>
            </div>
        </div>
    </div>
</div>

<?php
//$this->widget('bootstrap.widgets.TbDetailView', array(
//    'data' => $model,
//    'attributes' => array(
//        'id',
//        'name',
//        'title',
//        'description',
//        'text',
//        'is_active',
//        'image',
//    ),
//));
?>
