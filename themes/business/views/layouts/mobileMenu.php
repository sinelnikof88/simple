<ul id=" <?= (isset($main) ? "menu-main-menu" : ' ' ) ?>" class=" <?=
(isset($main) ?
        "nav navbar-nav gva-nav-menu gva-mobile-menu
        " : 'submenu-inner' )
?>">
        <? foreach ($data as $value): ?>
        <li class="<?= (isset($main) ? "menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-32 has-sub" : 'menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children' ) ?>">
            <?php if (strlen($value['url']) > 1): ?>
                <a href="<?= @$value['url'] ?>"><?= @$value['label'] ?>
                <?php else: ?>
                    <a><?= @$value['label'] ?>
                    <?php endif; ?>


                    <?= @$value['label'] ?></a>
                <?php if (isset($value['children']) && !empty($value['children'])): ?>
                    <?php $this->renderPartial('//layouts/mobileMenu', ['data' => $value['children']]); ?>
                <?php endif; ?>
        </li>
    <?php endforeach; ?>  
</ul>