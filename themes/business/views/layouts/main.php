<!DOCTYPE html>
<html lang="en-US" class="no-js">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta name="apple-touch-fullscreen" content="yes"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta name='robots' content='noindex,follow' />
        <link rel='dns-prefetch' href='//fonts.googleapis.com' />
        <link rel='dns-prefetch' href='//s.w.org' />
        <link rel="alternate" type="application/rss+xml" title="Winnex - Business Consulting WordPress Themes &raquo; Feed" href="/themes/business/?feed=rss2" />
        <link rel="alternate" type="application/rss+xml" title="Winnex - Business Consulting WordPress Themes &raquo; Comments Feed" href="/themes/business/?feed=comments-rss2" />
        <link rel='stylesheet' id='icon-custom-css'  href='/themes/business/css/icon-custom.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' id='contact-form-7-css'  href='/themes/business/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.5' type='text/css' media='all' />
        <link rel='stylesheet' id='js_composer_front-css'  href='/themes/business/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.5.5' type='text/css' media='all' />
        <link rel='stylesheet' id='winnex-style-css'  href='/themes/business/wp-content/themes/winnex/style.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' id='magnific-css'  href='/themes/business/js/magnific/magnific-popup.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' id='icon-fontawesome-css'  href='/themes/business/css/fontawesome/css/font-awesome.min.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' id='winnex-bootstrap-css'  href='/themes/business/css/bootstrap.css?ver=1.0.8' type='text/css' media='all' />
        <link rel='stylesheet' id='winnex-template-css'  href='/themes/business/css/template.css?ver=1.0.8' type='text/css' media='all' />
        <link rel='stylesheet' id='winnex-template-css'  href='/themes/business/css/rangeslider.css?ver=1.0.8' type='text/css' media='all' />
        <script type='text/javascript' src='/themes/business/js/jquery/jquery.js?ver=1.12.4'></script>
        <script type='text/javascript' src='/themes/business/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
        <script type='text/javascript' src='/themes/business/js/bootstrap.js?ver=4.9.8'></script>
        <script type='text/javascript' src='/themes/business/js/countdown.js?ver=4.9.8'></script>
        <script type='text/javascript' src='/themes/business/js/count-to.js?ver=4.9.8'></script>
        <script type='text/javascript' src='/themes/business/js/jquery.appear.js?ver=4.9.8'></script>
        <script type='text/javascript' src='/themes/business/js/perfect-scrollbar.jquery.min.js?ver=4.9.8'></script>
        <script type='text/javascript' src='/themes/business/js/magnific/jquery.magnific-popup.min.js?ver=4.9.8'></script>
        <script type='text/javascript' src='/themes/business/js/rangeslider.min.js?ver=11'></script>
        <script type='text/javascript' src='/themes/business/js/scroll/jquery.scrollto.js?ver=4.9.8'></script>
        <script type='text/javascript' src='/themes/business/js/waypoint.js?ver=4.9.8'></script>
        <script type='text/javascript' src='/themes/business/js/main.js?ver=4.9.8'></script>
        <meta name="generator" content="black-town.ru" />       
        <link rel="canonical" href="<?php echo CHtml::encode(yii::app()->config->siteName); ?>" />
        <link rel='shortlink' href='<?php echo CHtml::encode(yii::app()->config->siteName); ?>' />    
        <meta name="twitter:card" content="<?php echo CHtml::encode(yii::app()->config->icon); ?>" />
        <meta property="og:url"	content="<?php echo CHtml::encode(yii::app()->config->siteName); ?>" />
        <meta property="og:type"content="<?php echo CHtml::encode(yii::app()->config->description); ?>" />
        <meta property="og:title"content="<?php echo CHtml::encode($this->pageTitle); ?>" />
        <meta property="og:description" content="<?php echo CHtml::encode(yii::app()->config->description); ?>" />
        <meta property="og:image" content="<?php echo CHtml::encode(yii::app()->config->icon); ?>" />
        <script> var ajaxurl = "/themes/business/wp-admin/admin-ajax.php";</script>
        <style>
           

        </style>
        <script type="text/javascript">function setREVStartSize(e) {
                try {
                    e.c = jQuery(e.c);
                    var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                    if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                    }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                        var u = (e.c.width(), jQuery(window).height());
                        if (void 0 != e.fullScreenOffsetContainer) {
                            var c = e.fullScreenOffsetContainer.split(",");
                            if (c)
                                jQuery.each(c, function (e, i) {
                                    u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                                }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                        }
                        f = u
                    } else
                        void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                    e.c.closest(".rev_slider_wrapper").css({height: f})
                } catch (d) {
                    console.log("Failure at Presize of Slider:" + d)
                }
            }
        </script>
    </head>
    <body class="home page-template-default page page-id-36 woocommerce-no-js wpb-js-composer js-comp-ver-5.5.5 vc_responsive">
        <div class="wrapper-page"> <!--page-->

            <?php $this->renderPartial('//layouts/header') ?>
            <div id="page-content"> <!--page content-->
                <div id="wp-main-content" class="clearfix main-page title-layout-standard">
                    <div class="container-full">
                        <div class="content-page-wrap">
                            <div class="main-page-content base-layout row has-no-sidebar">
                                <div class="content-page col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="content-page-inner">   
                                        <div class="clearfix post-36 page type-page status-publish hentry" id="36">
                                            <? echo $content ?>
                                            <div class="link-pages">
                                            </div>
                                            <div class="container">
                                            </div>
                                        </div>
                                    </div>    
                                </div>      
                            </div> 
                        </div>
                    </div>
                </div>    
            </div>
            <!--end page content-->
        </div>
        <!-- End page -->
        <?php $this->renderPartial('//layouts/footer') ?>
        <div id="gva-overlay">
        </div>
        <div id="gva-quickview" class="clearfix">
        </div>
    </body>
</html>