     <? foreach ($data as $value): ?>
        <li class="menu-item menu-item-type-custom menu-item-object-custom">
            <a href="<?= @$value['url'] ?>"><?= @$value['label'] ?>
            <?php if (isset($value['children']) && !empty($value['children'])): ?>
                 <?php endif; ?>
            </a>
            <?php if (isset($value['children']) && !empty($value['children'])): ?>
                <?php $this->renderPartial('//layouts/menu_footer', ['data' => $value['children']]); ?>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>  
 
