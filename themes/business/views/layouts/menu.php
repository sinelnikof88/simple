<ul id=" <?= (isset($main) ? "menu-main-menu-1" : ' ' ) ?>" class=" <?= (isset($main) ? "nav navbar-nav gva-nav-menu gva-main-menu" : 'submenu-inner' ) ?>">
    <? foreach ($data as $value): ?>
        <li class="
            menu-item menu-item-type-post_type menu-item-object-product">
            <?php if (strlen($value['url'])>1): ?>
            <a href="<?= @$value['url'] ?>"><?= @$value['label'] ?>
            <?php else: ?>
                <a><?= @$value['label'] ?>
            <?php endif; ?>


                    <?php if (isset($value['children']) && !empty($value['children'])): ?>
                        <span class="caret"></span>     
                    <?php endif; ?>
                </a>
                <?php if (isset($value['children']) && !empty($value['children'])): ?>
                    <?php $this->renderPartial('//layouts/menu', ['data' => $value['children']]); ?>
                <?php endif; ?>
        </li>
    <?php endforeach; ?>  
</ul>

