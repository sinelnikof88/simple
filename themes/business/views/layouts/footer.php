<footer id="wp-footer" class="clearfix">
    <div class="footer-main">
        <div class="vc_wpb_row_inner  ">
            <div class="vc_row wpb_row vc_row-fluid remove_padding_bottom row-container">
                <div class="container">
                    <div class="row ">
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div  class="vc_wp_text wpb_content_element">
                                        <div class="widget widget_text">			<div class="textwidget">
                                                <p>
                                                    <img class="alignnone size-full wp-image-3548" src="<?= yii::app()->config->logo ?>" alt="" width="158" height="37" />
                                                </p>
                                                <ul class="contact-info">
                                                    <li>Контакты</li>
                                                    <li class="phone"><?= yii::app()->config->phone ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-5 vc_col-md-5 vc_col-xs-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div  class="vc_wp_custommenu wpb_content_element">
                                                        <div class="widget widget_nav_menu">
                                                            <h2 class="widgettitle">Разделы</h2>
                                                            <div class="menu-extras-container">
                                                                <ul id="menu-extras" class="menu">
                                                                    <?php
                                                                    $data = SortedMenu::model()->findByPk(1);
                                                                    $data = $data->items;
                                                                    if (!empty($data)) {
                                                                        $this->renderPartial('//layouts/menu_footer', ['data' => $data, 'main' => 1]);
                                                                    }
                                                                    ?> 
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div  class="vc_wp_custommenu wpb_content_element">
                                                        <div class="widget widget_nav_menu">
                                                            <h2 class="widgettitle">Займы под залог</h2>
                                                            <div class="menu-extras-container">
                                                                <ul id="menu-extras-1" class="menu">
                                                                    <?php foreach (Pages::model()->by('group_id', 1)->order('id desc')->findAll() as $value) : ?>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-683">
                                                                            <a href="<?=@$value->link?>"><?=@$value->name?></a>
                                                                        </li>
                                                                    <?php endforeach; ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-3 vc_col-xs-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <section class="widget gva-blogs-list section-blog ">
                                        <h3 class="widget-title visual-title">
                                            <span>Новости</span>
                                        </h3>

                                        <div class="widget-content">
                                            <div class="posts-list post-small post-items">

                                                <?php foreach (News::model()->order('id desc')->limit(2)->findAll() as $news): ?>
                                                    <?php $this->renderPartial('//layouts/news_footer', ['model' => $news]) ?>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>		
    <div class="copyright">
        <div class="container">
            <div class="copyright-inner">
                <div class="row">
                    
                </div>	
            </div>
        </div>
    </div>
    <div class="return-top default">
        <i class="gv-icon-194">
        </i>
    </div>

</footer>