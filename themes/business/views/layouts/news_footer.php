<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sticky-post">
        <article class="post-257 post type-post status-publish format-gallery has-post-thumbnail hentry category-fashion post_format-post-format-gallery">
            <div class="post-thumbnail">
                <img width="180" height="180" src="<?= @$model->image ?>" class="attachment-thumbnail size-thumbnail wp-post-image" alt="The best apps to your next trip" />          </div>  
            <div class="post-content">
                <div class="entry-header">
                    <h3 class="entry-title">
                        <a href="<?= @$model->link?>" rel="bookmark"><?= @$model->name ?></a>
                    </h3>  <div class="entry-meta">
                     
               
                    </div>
                </div>
            </div>  
        </article>

    </div>
</div>