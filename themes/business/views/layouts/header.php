

<header class=" header-default header-v1">

    <div class="gv-sticky-menu">

        <div class="header-mobile hidden-lg hidden-md">
            <div class="container">
                <div class="row">
                    <div class="left col-xs-4">
                        <div class="hidden-lg hidden-md">
                            <div class="canvas-menu gva-offcanvas">
                                <a class="dropdown-toggle" data-canvas=".mobile" href="#"><i class="gv-icon-103"></i></a>
                            </div>
                            <div class="gva-offcanvas-content mobile">
                                <div class="close-canvas"><a><i class="gv-icon-8"></i></a></div>
                                <div class="wp-sidebar sidebar ps-container" data-ps-id="bee7f770-0ec8-b3f3-d24c-0ce76b23f1d8">
                                    <div id="gva-mobile-menu" class="navbar-collapse">
                                        
                                         <?php
                                        $data = SortedMenu::model()->findByPk(1);
                                        $data = $data->items;
                                        if (!empty($data)) {
                                            $this->renderPartial('//layouts/mobileMenu', ['data' => $data, 'main' => 1]);
                                        }
                                        ?>
                                    </div>
                                  
                                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div></div></div>
                            </div>
                        </div> </div>
                    <div class="center text-center col-xs-4">
                        <div class="logo-menu">
                           <a class="logo-theme" href="/">
                                <img src="<?= yii::app()->config->logo ?>" alt="<?= yii::app()->config->logoText ?>" />
                            </a>
                        </div>
                    </div>
 
                </div>
            </div>
        </div> 


        <div class="header-mainmenu hidden-xs hidden-sm">
            <div class="container"> 
                <div class="prelative">
                    <div class="row">
                        <div class="logo col-lg-2 col-md-2 col-sm-12">
                            <a class="logo-theme" href="/">
                                <img src="<?= yii::app()->config->logo ?>" alt="<?= yii::app()->config->logoText ?>" />
                            </a>
                        </div>
                        <div class="col-sm-10 col-xs-12 pstatic header-right quick-header-enable">
                            <div class="content-innter clearfix">
                                <div id="gva-mainmenu" class="pstatic main-menu header-bottom">
                                    <div id="gva-main-menu" class="navbar-collapse">
                                        <?php
                                        $data = SortedMenu::model()->findByPk(1);
                                        $data = $data->items;
                                        if (!empty($data)) {
                                            $this->renderPartial('//layouts/menu', ['data' => $data, 'main' => 1]);
                                        }
                                        ?>
                                    </div>     
                                </div>
                                <div class="header-information">
                                    <div class="left">
                                        <span class="icon">
                                            <img src="http://themesgavias.com/wp/winnex/wp-content/themes/winnex/images/header-info-icon.png" alt="Call us anytime" />
                                        </span>
                                    </div>
                                    <div class="right">
                                        <span class="title">Контакты</span>
                                        <span class="text"><?= yii::app()->config->phone ?></span>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>  
                </div>  
            </div>
        </div>  
    </div> 
</header>
