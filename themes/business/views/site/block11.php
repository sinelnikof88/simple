

<div class="vc_wpb_row_inner  ">
    <div class="vc_row wpb_row vc_row-fluid row-container">
        <div class="container">
            <div class="row ">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="widget gsc-heading  align-center style-default text-dark">
                                <h2 class="title">
                                    <span>Tips &amp; News</span>
                                </h2>   <div class="line">
                                    <span>
                                    </span>
                                </div>
                                <div class="title-desc">Build strategies, build confidence, build your business.</div>
                            </div>

                            <div class="widget gva-view-mlayout-1 ">
                                <div class="widget-content">
                                    <div class="posts-grids post-items">
                                        <div class="item-first">
                                            <div class="post">
                                                <div class="post-thumbnail">
                                                    <img width="750" height="510" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2016/01/post-2-750x510.jpg" class="attachment-medium size-medium wp-post-image" alt="The best apps to your next trip" />                  </div>  
                                                <div class="post-content">
                                                    <div class="entry-meta">
                                                        <span class="cat-links">
                                                            <a href="http://themesgavias.com/wp/winnex/?cat=65" rel="category">Fashion</a>
                                                        </span>
                                                        <span class="line">
                                                        </span>
                                                        <span class="entry-date">
                                                            <a href="http://themesgavias.com/wp/winnex/?p=257" rel="bookmark">
                                                                <time class="entry-date" datetime="2016-01-03T15:30:10+00:00">January 3, 2016</time>
                                                            </a>
                                                        </span>                    </div>  
                                                    <h3 class="entry-title">
                                                        <a href="http://themesgavias.com/wp/winnex/?p=257" rel="bookmark">The best apps to your next trip</a>
                                                    </h3>                    <div class="post-body">It is a well-known fact that when any discussion on lead generation takes place, quality conquers over the measure. Though it is clearly known</div>
                                                </div>
                                            </div>
                                        </div>  

                                        <div class="items-second post-small-2">                  <article class="post-254 post type-post status-publish format-video has-post-thumbnail hentry category-lifestyle post_format-post-format-video">
                                                <div class="post-thumbnail">
                                                    <img width="180" height="180" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2016/01/post-8-1-180x180.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Highlights New York Fashion Week 2018" />                    </div>  
                                                <div class="post-content">
                                                    <div class="entry-header">
                                                        <div class="entry-meta">
                                                            <span class="cat-links">
                                                                <a href="http://themesgavias.com/wp/winnex/?cat=64" rel="category">LifeStyle</a>
                                                            </span>
                                                            <span class="line">
                                                            </span>
                                                            <span class="entry-date">
                                                                <a href="http://themesgavias.com/wp/winnex/?p=254" rel="bookmark">
                                                                    <time class="entry-date" datetime="2016-01-03T15:28:21+00:00">January 3, 2016</time>
                                                                </a>
                                                            </span>                        </div>
                                                        <h3 class="entry-title">
                                                            <a href="http://themesgavias.com/wp/winnex/?p=254" rel="bookmark">Highlights New York Fashion Week 2018</a>
                                                        </h3>                        <div class="post-body">It is a well-known fact that when any discussion on lead generation takes place, quality</div>
                                                    </div>
                                                </div>  
                                            </article>


                                            <article class="post-252 post type-post status-publish format-standard has-post-thumbnail hentry category-music">
                                                <div class="post-thumbnail">
                                                    <img width="180" height="180" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2016/01/post-4-180x180.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="How To Find Cheap Travel" />                    </div>  
                                                <div class="post-content">
                                                    <div class="entry-header">
                                                        <div class="entry-meta">
                                                            <span class="cat-links">
                                                                <a href="http://themesgavias.com/wp/winnex/?cat=66" rel="category">Music</a>
                                                            </span>
                                                            <span class="line">
                                                            </span>
                                                            <span class="entry-date">
                                                                <a href="http://themesgavias.com/wp/winnex/?p=252" rel="bookmark">
                                                                    <time class="entry-date" datetime="2016-01-03T15:27:38+00:00">January 3, 2016</time>
                                                                </a>
                                                            </span>                        </div>
                                                        <h3 class="entry-title">
                                                            <a href="http://themesgavias.com/wp/winnex/?p=252" rel="bookmark">How To Find Cheap Travel</a>
                                                        </h3>                        <div class="post-body">It is a well-known fact that when any discussion on lead generation takes place, quality</div>
                                                    </div>
                                                </div>  
                                            </article>


                                            <article class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-movies">
                                                <div class="post-thumbnail">
                                                    <img width="180" height="180" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2016/01/post-5-180x180.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Latest Fashion Trends in This summer" />                    </div>  
                                                <div class="post-content">
                                                    <div class="entry-header">
                                                        <div class="entry-meta">
                                                            <span class="cat-links">
                                                                <a href="http://themesgavias.com/wp/winnex/?cat=67" rel="category">Movies</a>
                                                            </span>
                                                            <span class="line">
                                                            </span>
                                                            <span class="entry-date">
                                                                <a href="http://themesgavias.com/wp/winnex/?p=242" rel="bookmark">
                                                                    <time class="entry-date" datetime="2016-01-03T15:27:02+00:00">January 3, 2016</time>
                                                                </a>
                                                            </span>                        </div>
                                                        <h3 class="entry-title">
                                                            <a href="http://themesgavias.com/wp/winnex/?p=242" rel="bookmark">Latest Fashion Trends in This summer</a>
                                                        </h3>                        <div class="post-body">It is a well-known fact that when any discussion on lead generation takes place, quality</div>
                                                    </div>
                                                </div>  
                                            </article>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>