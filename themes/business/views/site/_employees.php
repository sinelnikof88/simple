 
     <?php
    $model = Employees::model()->order('id desc')->findAll();
    foreach ($model as $data) {
        $data->swiperContent = $this->renderPartial('_employees_item', ['data' => $data], true, false);
    }

    $this->widget('ext.swiper.HtmlSwiper', array(
        'htmlData' => $model,
        'slideNumber' => [
            'big' => 5,
            'medium' => 5,
            'small' => 1,
        ],
        'options' => [
            'paginationClickable' => true,
            'slidesPerView' => 4,
            'spaceBetween' => 30,
//            'slidesPerGroup' => 4,
            'loop' => true,
        ]
    ));
    ?>
 