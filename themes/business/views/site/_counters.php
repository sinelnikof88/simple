
<div class="vc_wpb_row_inner  ">
    <div class="vc_row wpb_row vc_row-fluid remove_padding_top row-container">
        <div class="container">
            <div class="row ">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget gsc-heading  align-center style-3 text-dark">
                                                <h2 class="title">
                                                    <span><?= @$model->_value['title'] ?></span>
                                                </h2>   <div class="line">
                                                    <span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1539141626394">
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget milestone-block  position-number-top text-dark">

                                                <div class="milestone-right">
                                                    <div class="milestone-number-inner" >
                                                        <span class="milestone-number"><?= @$model->_value['item1'] ?></span>
                                                        <span class="symbol"><?= @$model->_value['item1-symbol'] ?></span>
                                                    </div>
                                                    <div class="milestone-text"><?= @$model->_value['item1-text'] ?></div>
                                                </div>
                                            </div> </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget milestone-block  position-number-top text-dark">

                                               <div class="milestone-right">
                                                    <div class="milestone-number-inner" >
                                                        <span class="milestone-number"><?= @$model->_value['item2'] ?></span>
                                                        <span class="symbol"><?= @$model->_value['item2-symbol'] ?></span>
                                                    </div>
                                                    <div class="milestone-text"><?= @$model->_value['item2-text'] ?></div>
                                                </div>
                                            </div> </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget milestone-block  position-number-top text-dark">

                                               <div class="milestone-right">
                                                    <div class="milestone-number-inner" >
                                                        <span class="milestone-number"><?= @$model->_value['item3'] ?></span>
                                                        <span class="symbol"><?= @$model->_value['item3-symbol'] ?></span>
                                                    </div>
                                                    <div class="milestone-text"><?= @$model->_value['item3-text'] ?></div>
                                                </div>
                                            </div> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>