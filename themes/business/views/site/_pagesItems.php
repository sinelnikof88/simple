     <div class="service-block service-item-v1">
        <div class="service-images lightGallery">
            <div class="image-item">
                <img width="500" height="375"
                     src="/site/resized/400x280/<?= $data->image ?>" 
                     class="attachment-winnex_medium size-winnex_medium" 
                     alt=""> 
            </div>
        </div>
        <div class="service-content" style="             min-height: 100px;             ">
            <div class="content-inner">
                <div class="content-right">
                    <a href="<?= $data->link ?>">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
                <div class="content-left">
                    <h3 class="title " style=" height: 43px; ">
                        <a href="<?= $data->link ?>">
                            <?= $data->name ?>
                        </a>
                    </h3>
                    <div class="desc" style=" height: 43px; "><?= $data->description ?></div>
                </div>
            </div>
        </div>


    </div>
 