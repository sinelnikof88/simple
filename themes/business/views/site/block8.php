
<div class="vc_wpb_row_inner  ">
    <div class="vc_row wpb_row vc_row-fluid row-container">
        <div class="container">
            <div class="row ">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="widget gsc-heading  align-center style-default text-dark">
                                <h2 class="title">
                                    <span>More than 25 years experience</span>
                                </h2>   <div class="line">
                                    <span>
                                    </span>
                                </div>
                                <div class="title-desc">Build strategies, build confidence, build your business.</div>
                            </div>
                            <div class="gsc-service-carousel ">
                                <div class="gva-service-items services-1 clearfix">
                                    <div class="init-carousel-owl owl-carousel" data-items="3" data-items_lg="3" data-items_md="3" data-items_sm="2" data-items_xs="2" data-loop="1" data-speed="1000" data-auto_play="1" data-auto_play_speed="1000" data-auto_play_timeout="3000" data-auto_play_hover="1" data-navigation="1" data-rewind_nav="1" data-pagination="0" data-mouse_drag="1" data-touch_drag="1" >
                                        <div class="item">
                                            <div class="service-block service-item-v1">      
                                                <div class="service-images lightGallery">
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-6.jpg" class="zoomGallery" data-rel="lightGallery">
                                                            <span class="icon-expand">
                                                                <i class="fa fa-expand">
                                                                </i>
                                                            </span>
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-6-180x180.jpg"  class="hidden" alt="Finance and Restructuring" />
                                                        </a>
                                                        <img width="500" height="375" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-6-500x375.jpg" class="attachment-winnex_medium size-winnex_medium" alt="" />                  </div>
                                                </div>   
                                                <div class="service-content">
                                                    <div class="content-inner">
                                                        <div class="content-right">
                                                            <a href="http://themesgavias.com/wp/winnex/?service=finance-and-restructuring">
                                                                <i class="fa fa-angle-right">
                                                                </i>
                                                            </a>
                                                        </div>
                                                        <div class="content-left">
                                                            <h3 class="title">
                                                                <a href="http://themesgavias.com/wp/winnex/?service=finance-and-restructuring">Finance and Restructuring</a>
                                                            </h3>
                                                            <div class="desc">We work closely with Employers</div> 
                                                        </div>   
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>                          <div class="item">
                                            <div class="service-block service-item-v1">      
                                                <div class="service-images lightGallery">
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-4.jpg" class="zoomGallery" data-rel="lightGallery">
                                                            <span class="icon-expand">
                                                                <i class="fa fa-expand">
                                                                </i>
                                                            </span>
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-4-180x180.jpg"  class="hidden" alt="Audit and Evaluation" />
                                                        </a>
                                                        <img width="500" height="375" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-4-500x375.jpg" class="attachment-winnex_medium size-winnex_medium" alt="" />                  </div>
                                                </div>   
                                                <div class="service-content">
                                                    <div class="content-inner">
                                                        <div class="content-right">
                                                            <a href="http://themesgavias.com/wp/winnex/?service=audit-and-evaluation">
                                                                <i class="fa fa-angle-right">
                                                                </i>
                                                            </a>
                                                        </div>
                                                        <div class="content-left">
                                                            <h3 class="title">
                                                                <a href="http://themesgavias.com/wp/winnex/?service=audit-and-evaluation">Audit and Evaluation</a>
                                                            </h3>
                                                            <div class="desc">We work closely with Employers</div> 
                                                        </div>   
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>                          <div class="item">
                                            <div class="service-block service-item-v1">      
                                                <div class="service-images lightGallery">
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-3.jpg" class="zoomGallery" data-rel="lightGallery">
                                                            <span class="icon-expand">
                                                                <i class="fa fa-expand">
                                                                </i>
                                                            </span>
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-3-180x180.jpg"  class="hidden" alt="Taxes And Efficiency" />
                                                        </a>
                                                        <img width="500" height="375" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-3-500x375.jpg" class="attachment-winnex_medium size-winnex_medium" alt="" />                  </div>
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-2.jpg" class="zoomGallery hidden" data-rel="lightGallery">
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-2-180x180.jpg" alt="Taxes And Efficiency" class="hidden" />
                                                        </a>
                                                    </div>
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-4.jpg" class="zoomGallery hidden" data-rel="lightGallery">
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-4-180x180.jpg" alt="Taxes And Efficiency" class="hidden" />
                                                        </a>
                                                    </div>
                                                </div>   
                                                <div class="service-content">
                                                    <div class="content-inner">
                                                        <div class="content-right">
                                                            <a href="http://themesgavias.com/wp/winnex/?service=taxes-andefficiency">
                                                                <i class="fa fa-angle-right">
                                                                </i>
                                                            </a>
                                                        </div>
                                                        <div class="content-left">
                                                            <h3 class="title">
                                                                <a href="http://themesgavias.com/wp/winnex/?service=taxes-andefficiency">Taxes And Efficiency</a>
                                                            </h3>
                                                            <div class="desc">We work closely with Employers</div> 
                                                        </div>   
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>                          <div class="item">
                                            <div class="service-block service-item-v1">      
                                                <div class="service-images lightGallery">
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-5.jpg" class="zoomGallery" data-rel="lightGallery">
                                                            <span class="icon-expand">
                                                                <i class="fa fa-expand">
                                                                </i>
                                                            </span>
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-5-180x180.jpg"  class="hidden" alt="Financial Analysis" />
                                                        </a>
                                                        <img width="500" height="375" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-5-500x375.jpg" class="attachment-winnex_medium size-winnex_medium" alt="" />                  </div>
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-6.jpg" class="zoomGallery hidden" data-rel="lightGallery">
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-6-180x180.jpg" alt="Financial Analysis" class="hidden" />
                                                        </a>
                                                    </div>
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-3.jpg" class="zoomGallery hidden" data-rel="lightGallery">
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-3-180x180.jpg" alt="Financial Analysis" class="hidden" />
                                                        </a>
                                                    </div>
                                                </div>   
                                                <div class="service-content">
                                                    <div class="content-inner">
                                                        <div class="content-right">
                                                            <a href="http://themesgavias.com/wp/winnex/?service=financial-analysis">
                                                                <i class="fa fa-angle-right">
                                                                </i>
                                                            </a>
                                                        </div>
                                                        <div class="content-left">
                                                            <h3 class="title">
                                                                <a href="http://themesgavias.com/wp/winnex/?service=financial-analysis">Financial Analysis</a>
                                                            </h3>
                                                            <div class="desc">We work closely with Employers</div> 
                                                        </div>   
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>                          <div class="item">
                                            <div class="service-block service-item-v1">      
                                                <div class="service-images lightGallery">
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-1.jpg" class="zoomGallery" data-rel="lightGallery">
                                                            <span class="icon-expand">
                                                                <i class="fa fa-expand">
                                                                </i>
                                                            </span>
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-1-180x180.jpg"  class="hidden" alt="Consumer Products" />
                                                        </a>
                                                        <img width="500" height="375" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-1-500x375.jpg" class="attachment-winnex_medium size-winnex_medium" alt="" />                  </div>
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-5.jpg" class="zoomGallery hidden" data-rel="lightGallery">
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-5-180x180.jpg" alt="Consumer Products" class="hidden" />
                                                        </a>
                                                    </div>
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-3.jpg" class="zoomGallery hidden" data-rel="lightGallery">
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-3-180x180.jpg" alt="Consumer Products" class="hidden" />
                                                        </a>
                                                    </div>
                                                </div>   
                                                <div class="service-content">
                                                    <div class="content-inner">
                                                        <div class="content-right">
                                                            <a href="http://themesgavias.com/wp/winnex/?service=consumer-products">
                                                                <i class="fa fa-angle-right">
                                                                </i>
                                                            </a>
                                                        </div>
                                                        <div class="content-left">
                                                            <h3 class="title">
                                                                <a href="http://themesgavias.com/wp/winnex/?service=consumer-products">Consumer Products</a>
                                                            </h3>
                                                            <div class="desc">We work closely with Employers</div> 
                                                        </div>   
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>                          <div class="item">
                                            <div class="service-block service-item-v1">      
                                                <div class="service-images lightGallery">
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-2.jpg" class="zoomGallery" data-rel="lightGallery">
                                                            <span class="icon-expand">
                                                                <i class="fa fa-expand">
                                                                </i>
                                                            </span>
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-2-180x180.jpg"  class="hidden" alt="Energy &#038; Environment" />
                                                        </a>
                                                        <img width="500" height="375" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-2-500x375.jpg" class="attachment-winnex_medium size-winnex_medium" alt="" />                  </div>
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-3.jpg" class="zoomGallery hidden" data-rel="lightGallery">
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-3-180x180.jpg" alt="Energy &#038; Environment" class="hidden" />
                                                        </a>
                                                    </div>
                                                    <div class="image-item">
                                                        <a href="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-1.jpg" class="zoomGallery hidden" data-rel="lightGallery">
                                                            <img src="http://themesgavias.com/wp/winnex/wp-content/uploads/2015/12/service-1-180x180.jpg" alt="Energy &#038; Environment" class="hidden" />
                                                        </a>
                                                    </div>
                                                </div>   
                                                <div class="service-content">
                                                    <div class="content-inner">
                                                        <div class="content-right">
                                                            <a href="http://themesgavias.com/wp/winnex/?service=energy-environment">
                                                                <i class="fa fa-angle-right">
                                                                </i>
                                                            </a>
                                                        </div>
                                                        <div class="content-left">
                                                            <h3 class="title">
                                                                <a href="http://themesgavias.com/wp/winnex/?service=energy-environment">Energy &#038; Environment</a>
                                                            </h3>
                                                            <div class="desc">We work closely with Employers</div> 
                                                        </div>   
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>                     </div>
                                </div> 

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>