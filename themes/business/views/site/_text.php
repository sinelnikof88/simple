<div class="vc_wpb_row_inner  vc_custom_1542301678784">
    <div style=" background-image: url('<?= @$model->_value['background']?>');" class=" parallax vc_row wpb_row vc_row-fluid padding-small row-container vc_general vc_parallax vc_parallax-content-moving">
        <div class="container">
            <div class="row ">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="widget gsc-call-to-action  button-center text-light" style="max-width: 990px;">
                                <div class=" content-inner clearfix" >
                                    <div class="content">
                                        <h2 class="title">
                                            <span><?= @$model->_value['title']?></span>
                                        </h2>
                                        <div class="desc"><?= @$model->_value['text']?></div>      </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>