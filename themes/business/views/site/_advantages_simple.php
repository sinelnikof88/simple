
<div class="vc_wpb_row_inner  ">
    <div class="vc_row wpb_row vc_row-fluid row-container vc_row-o-equal-height vc_row-flex">
        <div class="container">
            <div class="row vc_row vc_row-o-equal-height vc_row-flex wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget gsc-heading  align-center style-default text-dark">
                                                <h2 class="title">
                                                    <span><?= @$model->_value['title'] ?></span>
                                                </h2>   <div class="line">
                                                    <span>
                                                    </span>
                                                </div>
                                                <div class="title-desc"><?= @$model->_value['description'] ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget gsc-icon-box text-dark left  margin-bottom-2" >
                                                <div class="highlight-icon">
                                                    <span class="icon-image">
                                                        <img src=" <?= @$model->_value['image1'] ?>"/> </span>
                                                </div>
                                                <div class="highlight_content">
                                                    <div class="title">
                                                        <a href="#"> <?= @$model->_value['title1'] ?>        </a>     
                                                    </div>
                                                    <div class="desc"><?= @$model->_value['text1'] ?> </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                          <div class="widget gsc-icon-box text-dark left  margin-bottom-2" >
                                                <div class="highlight-icon">
                                                    <span class="icon-image">
                                                        <img src=" <?= @$model->_value['image2'] ?>"/> </span>
                                                </div>
                                                <div class="highlight_content">
                                                    <div class="title">
                                                        <a href="#"> <?= @$model->_value['title2'] ?>        </a>     
                                                    </div>
                                                    <div class="desc"><?= @$model->_value['text2'] ?> </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget gsc-icon-box text-dark left  margin-bottom-1" >
                                                <div class="highlight-icon">
                                                    <span class="icon-image">
                                                        <img src=" <?= @$model->_value['image3'] ?>"/> </span>
                                                </div>
                                                <div class="highlight_content">
                                                    <div class="title">
                                                        <a href="#"> <?= @$model->_value['title3'] ?>        </a>     
                                                    </div>
                                                    <div class="desc"><?= @$model->_value['text3'] ?> </div>
                                                </div>
                                            </div> 


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget gsc-icon-box text-dark left " >
                                                 <div class="highlight-icon">
                                                    <span class="icon-image">
                                                        <img src=" <?= @$model->_value['image4'] ?>"/> </span>
                                                </div>
                                                <div class="highlight_content">
                                                    <div class="title">
                                                        <a href="#"> <?= @$model->_value['title4'] ?>        </a>     
                                                    </div>
                                                    <div class="desc"><?= @$model->_value['text4'] ?> </div>
                                                </div>

                                            </div> 


                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget gsc-icon-box text-dark left " >
                                                  <div class="highlight-icon">
                                                    <span class="icon-image">
                                                        <img src=" <?= @$model->_value['image5'] ?>"/> </span>
                                                </div>
                                                <div class="highlight_content">
                                                    <div class="title">
                                                        <a href="#"> <?= @$model->_value['title5'] ?>        </a>     
                                                    </div>
                                                    <div class="desc"><?= @$model->_value['text5'] ?> </div>
                                                </div>

                                            </div> 


                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget gsc-icon-box text-dark left " >
                                                  <div class="highlight-icon">
                                                    <span class="icon-image">
                                                        <img src=" <?= @$model->_value['image6'] ?>"/> </span>
                                                </div>
                                                <div class="highlight_content">
                                                    <div class="title">
                                                        <a href="#"> <?= @$model->_value['title6'] ?>        </a>     
                                                    </div>
                                                    <div class="desc"><?= @$model->_value['text6'] ?> </div>
                                                </div>

                                            </div> 


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>