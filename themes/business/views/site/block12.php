<div class="vc_wpb_row_inner  ">
    <div class="vc_row wpb_row vc_row-fluid remove_padding row-fullwidth">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="gsc-map ">
                        <div id="map_canvas_h619" class="map_canvas" style="width:100%; height:630px;">
                        </div>
                        <div class="content-inner clearfix">
                            <div class="link-close-content">
                                <a href="#">
                                    <i class="fa fa-times">
                                    </i>
                                </a>
                            </div>
                            <div class="content-inner-inner">
                                <h2 class="title">Send Us A Message</h2>
                                <div role="form" class="wpcf7" id="wpcf7-f236-p36-o2" lang="en-US" dir="ltr">
                                    <div class="screen-reader-response">
                                    </div>
                                    <form action="/wp/winnex/#wpcf7-f236-p36-o2" method="post" class="wpcf7-form" novalidate="novalidate">
                                        <div style="display: none;">
                                            <input type="hidden" name="_wpcf7" value="236" />
                                            <input type="hidden" name="_wpcf7_version" value="5.0.5" />
                                            <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f236-p36-o2" />
                                            <input type="hidden" name="_wpcf7_container_post" value="36" />
                                        </div>
                                        <div class="contact-feedback-form">
                                            <div class="row">
                                                <div class="col-sm-4 col-xs-12">
                                                    <span class="wpcf7-form-control-wrap your-name">
                                                        <input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your Name *" />
                                                    </span> </div>
                                                <div class="col-sm-4 col-xs-12">
                                                    <span class="wpcf7-form-control-wrap your-email">
                                                        <input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your Email *" />
                                                    </span> </div>
                                                <div class="col-sm-4 col-xs-12">
                                                    <span class="wpcf7-form-control-wrap your-subject">
                                                        <input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Your Subject" />
                                                    </span> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="wpcf7-form-control-wrap rows6">
                                                        <textarea name="rows:6" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Your message">
                                                        </textarea>
                                                    </span> </div>
                                                <div class="col-xs-12 action">
                                                    <div class="form-note">
                                                        <strong>Note:</strong> Your email address will not be published.</div>
                                                    <div class="form-action">
                                                        <input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpcf7-response-output wpcf7-display-none">
                                        </div>
                                    </form>
                                </div>         </div>   
                        </div>
                        <div class="link-open-content">
                            <a href="#">
                                <i class="fa fa-map-o">
                                </i>
                            </a>
                        </div>
                    </div>            
                    <script>
                        jQuery(document).ready(function ($) {
                            var stmapdefault = '38.546970, -89.965742';
                            var marker = {position: stmapdefault}
                            var content = '';
                            jQuery('#map_canvas_h619').gmap({
                                'scrollwheel': false,
                                'zoom': 14,
                                'center': stmapdefault,
                                'mapTypeId': google.maps.MapTypeId.ROADMAP,
                                'callback': function () {
                                    var self = this;
                                    self.addMarker(marker).on('click', function () {
                                        if (content) {
                                            self.openInfoWindow({'content': content}, self.instance.markers[0]);
                                        }
                                    });
                                },
                                panControl: true
                            });
                        });</script>
                </div>
            </div>
        </div>
    </div>
</div>
