<?php
$models = Block::model()->order('sort asc')->findAll();
$this->renderPartial('_slider');

foreach (($models) as $model) {
    $this->renderPartial($model->type, ['model' => $model]);
};
$this->renderPartial('_employees', ['data' => []]);

$news = News::model()->order('id desc')->limit(4)->findAll();
$this->renderPartial('_news', ['data' => $news]);
$this->renderPartial('_pages', ['data' => []]);

/* $this->renderPartial('block1') ?>        
  <?php $this->renderPartial('block3') ?>
  <?php $this->renderPartial('block5') ?>
  <?php $this->renderPartial('block6') ?>
  <?php $this->renderPartial('block7') ?>
  <?php $this->renderPartial('block8') ?>
  <?php $this->renderPartial('block10') ?>
  <?php $this->renderPartial('block11') */
?>  

<div class="container">
    <div class="row vc_row vc_row-o-equal-height vc_row-flex wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">

                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <? $this->widget('ext.calculator.CalculatorWiget'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>