<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<div class="vc_wpb_row_inner  vc_custom_1542301692942">

    <div data-vc-parallax="1.5" data-vc-parallax-image="http://themesgavias.com/wp/winnex/wp-content/uploads/2018/10/bg-2.jpg" class="vc_row wpb_row vc_row-fluid padding-small row-container vc_general vc_parallax vc_parallax-content-moving">
        <div class="container">
            <div class="row ">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="content-inner clearfix">
                                <div class="content" style=" text-align:  center">
                                    <h2 class="title">
                                        Login                                        </h2>
                                    <div class="desc" >



                                        <p>Please fill out the following form with your login credentials:</p>

                                        <div class="form">
                                            <?php
                                            $form = $this->beginWidget('CActiveForm', array(
                                                'id' => 'login-form',
                                                'enableClientValidation' => true,
                                                'clientOptions' => array(
                                                    'validateOnSubmit' => true,
                                                ),
                                            ));
                                            ?>

                                            <p class="note">Fields with <span class="required">*</span> are required.</p>

                                            <div class="row">
                                                <?php echo $form->labelEx($model, 'username'); ?>
                                                <?php echo $form->textField($model, 'username'); ?>
                                                <?php echo $form->error($model, 'username'); ?>
                                            </div>

                                            <div class="row">
                                                <?php echo $form->labelEx($model, 'password'); ?>
                                                <?php echo $form->passwordField($model, 'password'); ?>
                                                <?php echo $form->error($model, 'password'); ?>

                                            </div>


                                            <div class="row buttons">
                                                <?php echo CHtml::submitButton('Login'); ?>
                                            </div>

                                            <?php $this->endWidget(); ?>
                                        </div><!-- form -->









                                    </div>      </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

