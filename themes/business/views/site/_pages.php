<div class="vc_wpb_row_inner  ">
    <div class="vc_row wpb_row vc_row-fluid row-container">
        <div class="container">
            <div class="row ">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="widget gsc-heading  align-center style-default text-dark">
                                <h2 class="title"><span>Выдаем займы под залог:</span></h2>
                                <div class="line"><span></span></div>
                                <!--<div class="title-desc">Build strategies, build confidence, build your business.</div>-->
                            </div>
                            <div class="gsc-service-carousel ">
                                <div class="gva-service-items services-1 clearfix">
                                    <?php
                                    $model = Pages::model()->by('group_id', 1)->order('id desc')->findAll();
                                    foreach ($model as $data) {
                                        $data->swiperContent = $this->renderPartial('_pagesItems', ['data' => $data], true, false);
                                    }

                                    $this->widget('ext.swiper.HtmlSwiper', array(
                                        'htmlData' => $model,
                                        'slideNumber' => [
                                            'big' => 3,
                                            'medium' => 1,
                                            'small' => 1,
                                        ],
                                        'options' => [
                                            'slidesPerView' => 3,
//                                            'slidesPerGroup' => 3,
                                        ]
                                    ));
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>