<div class="vc_wpb_row_inner hidden-xs ">
    <div class="vc_row wpb_row vc_row-fluid remove_padding row-fullwidth">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="lp_bl swp" style="width: 100%">
                        <div class="txt_wrapper">   
                            <?php
                            $model = Slider::model()->order('sort asc')->findAll();
                            $this->widget('ext.swiper.Swiper', array(
                                'dataProvider' => new CArrayDataProvider($model),
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>