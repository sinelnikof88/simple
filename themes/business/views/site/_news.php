<div class="vc_wpb_row_inner  ">
    <div class="vc_row wpb_row vc_row-fluid row-container">
        <div class="container"><div class="row ">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="widget gsc-heading  align-center style-default text-dark">
                                <h2 class="title"><span>Новости</span></h2>
                                <div class="line"><span></span></div>
                                <!--<div class="title-desc">Build strategies, build confidence, build your business.</div>-->
                            </div>
                            <div class="widget gva-view-mlayout-1 ">
                                <div class="widget-content">
                                    <div class="posts-grids post-items">
                                        <div class="item-first">
                                            <div class="post">
                                                <div class="post-thumbnail">
                                                    <img width="750" height="510" src="<?= @$data[0]->image ?>" class="attachment-medium size-medium wp-post-image" alt=""> </div>
                                                <div class="post-content">
                                                    <div class="entry-meta">
                                                        <h3 class="entry-title"><a href="<?= @$data[0]->link ?>" rel="bookmark"><?= @$data[0]->name ?></a></h3> 
                                                        <div class="post-body"><?= @$data[0]->description ?></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="items-second post-small-2"> 
                                            <article class="post-254 post type-post status-publish format-video has-post-thumbnail hentry category-lifestyle post_format-post-format-video">
                                                <div class="post-thumbnail">
                                                    <img width="180" height="180" src="<?= @$data[1]->image ?>" class="attachment-thumbnail size-thumbnail wp-post-image" alt=""> </div>
                                                <div class="post-content">
                                                    <div class="entry-header">
                                                        <div class="entry-meta">
                                                            <h3 class="entry-title"><a href="<?= @$data[1]->link ?>" rel="bookmark"><?= @$data[1]->name ?></a></h3> 
                                                            <div class="post-body"><?= @$data[1]->description ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <article class="post-252 post type-post status-publish format-standard has-post-thumbnail hentry category-music">
                                                <div class="post-thumbnail">
                                                    <img width="180" height="180" src="<?= @$data[2]->image ?>" class="attachment-thumbnail size-thumbnail wp-post-image" alt=""> </div>
                                                <div class="post-content">
                                                    <div class="entry-header">
                                                        <div class="entry-meta">
                                                            <h3 class="entry-title"><a href="<?= @$data[2]->link ?>" rel="bookmark"><?= @$data[2]->name ?></a></h3>
                                                            <div class="post-body"><?= @$data[2]->description ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <article class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-movies">
                                                <div class="post-thumbnail">
                                                    <img width="180" height="180" src="<?= @$data[3]->image ?>" class="attachment-thumbnail size-thumbnail wp-post-image" alt=""> </div>
                                                <div class="post-content">
                                                    <div class="entry-header">
                                                        <div class="entry-meta">
                                                            <h3 class="entry-title"><a href="<?= @$data[3]->link ?>" rel="bookmark"><?= @$data[3]->name ?></a></h3> 
                                                            <div class="post-body"<?= @$data[3]->description ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>