    <div class="vc_wpb_row_inner  vc_custom_1539057673553">
                                                            <div class="vc_row wpb_row vc_row-fluid row-container">
                                                                <div class="container">
                                                                    <div class="row ">
                                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                            <div class="vc_column-inner">
                                                                                <div class="wpb_wrapper">
                                                                                    <div class="widget gsc-heading  align-center style-default text-dark">
                                                                                        <h2 class="title">
                                                                                            <span>Our Case Study</span>
                                                                                        </h2>   <div class="line">
                                                                                            <span>
                                                                                            </span>
                                                                                        </div>
                                                                                        <div class="title-desc">Build strategies, build confidence, build your business.</div>
                                                                                    </div>
                                                                                    <div class="gsc-portfolio-carousel clearfix ">
                                                                                        <div class="gva-portfolio-items no-gutter clearfix">
                                                                                            <div class="init-carousel-owl owl-carousel" data-items="3" data-items_lg="3" data-items_md="3" data-items_sm="2" data-items_xs="2" data-loop="1" data-speed="1000" data-auto_play="1" data-auto_play_speed="1000" data-auto_play_timeout="3000" data-auto_play_hover="1" data-navigation="1" data-rewind_nav="1" data-pagination="1" data-mouse_drag="1" data-touch_drag="1" >
                                                                                                <div class="item-columns all financial-analysis ">
                                                                                                    <div class="portfolio-block portfolio-v1 isotope-item grid">      
                                                                                                        <div class="portfolio-content">
                                                                                                            <div class="images">
                                                                                                                <a class="link-image-content" href="http://themesgavias.com/wp/winnex/?portfolio=hosting-website-rank">
                                                                                                                    <img width="500" height="375" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2017/03/portfolio-1-500x375.jpg" class="attachment-winnex_medium size-winnex_medium wp-post-image" alt="" />            </a>
                                                                                                                <a class="link" href="http://themesgavias.com/wp/winnex/?portfolio=hosting-website-rank">
                                                                                                                    <i class="gv-icon-809">
                                                                                                                    </i>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="content-inner">
                                                                                                                <div class="title">
                                                                                                                    <a href="http://themesgavias.com/wp/winnex/?portfolio=hosting-website-rank">Hosting Website Rank</a>
                                                                                                                </div> 
                                                                                                                <div class="category">
                                                                                                                    <a href="http://themesgavias.com/wp/winnex/?category_portfolio=financial-analysis" title="View all posts in Financial Analysis">Financial Analysis</a>
                                                                                                                </div>  
                                                                                                            </div>    
                                                                                                        </div>   
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="item-columns all energy-environment ">
                                                                                                    <div class="portfolio-block portfolio-v1 isotope-item grid">      
                                                                                                        <div class="portfolio-content">
                                                                                                            <div class="images">
                                                                                                                <a class="link-image-content" href="http://themesgavias.com/wp/winnex/?portfolio=cloaking-doorway-pages">
                                                                                                                    <img width="500" height="375" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2017/03/portfolio-2-500x375.jpg" class="attachment-winnex_medium size-winnex_medium wp-post-image" alt="" />            </a>
                                                                                                                <a class="link" href="http://themesgavias.com/wp/winnex/?portfolio=cloaking-doorway-pages">
                                                                                                                    <i class="gv-icon-809">
                                                                                                                    </i>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="content-inner">
                                                                                                                <div class="title">
                                                                                                                    <a href="http://themesgavias.com/wp/winnex/?portfolio=cloaking-doorway-pages">Cloaking &#038; Doorway Pages</a>
                                                                                                                </div> 
                                                                                                                <div class="category">
                                                                                                                    <a href="http://themesgavias.com/wp/winnex/?category_portfolio=energy-environment" title="View all posts in Energy &amp; Environment">Energy &amp; Environment</a>
                                                                                                                </div>  
                                                                                                            </div>    
                                                                                                        </div>   
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="item-columns all consumer-products ">
                                                                                                    <div class="portfolio-block portfolio-v1 isotope-item grid">      
                                                                                                        <div class="portfolio-content">
                                                                                                            <div class="images">
                                                                                                                <a class="link-image-content" href="http://themesgavias.com/wp/winnex/?portfolio=vr-solution-development">
                                                                                                                    <img width="500" height="375" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2017/03/portfolio-6-500x375.jpg" class="attachment-winnex_medium size-winnex_medium wp-post-image" alt="" />            </a>
                                                                                                                <a class="link" href="http://themesgavias.com/wp/winnex/?portfolio=vr-solution-development">
                                                                                                                    <i class="gv-icon-809">
                                                                                                                    </i>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="content-inner">
                                                                                                                <div class="title">
                                                                                                                    <a href="http://themesgavias.com/wp/winnex/?portfolio=vr-solution-development">VR Solution Development</a>
                                                                                                                </div> 
                                                                                                                <div class="category">
                                                                                                                    <a href="http://themesgavias.com/wp/winnex/?category_portfolio=consumer-products" title="View all posts in Consumer Products">Consumer Products</a>
                                                                                                                </div>  
                                                                                                            </div>    
                                                                                                        </div>   
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="item-columns all transport-avaition ">
                                                                                                    <div class="portfolio-block portfolio-v1 isotope-item grid">      
                                                                                                        <div class="portfolio-content">
                                                                                                            <div class="images">
                                                                                                                <a class="link-image-content" href="http://themesgavias.com/wp/winnex/?portfolio=optimizing-manufacturing">
                                                                                                                    <img width="500" height="375" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2017/03/portfolio-8-500x375.jpg" class="attachment-winnex_medium size-winnex_medium wp-post-image" alt="" />            </a>
                                                                                                                <a class="link" href="http://themesgavias.com/wp/winnex/?portfolio=optimizing-manufacturing">
                                                                                                                    <i class="gv-icon-809">
                                                                                                                    </i>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="content-inner">
                                                                                                                <div class="title">
                                                                                                                    <a href="http://themesgavias.com/wp/winnex/?portfolio=optimizing-manufacturing">Optimizing Manufacturing</a>
                                                                                                                </div> 
                                                                                                                <div class="category">
                                                                                                                    <a href="http://themesgavias.com/wp/winnex/?category_portfolio=transport-avaition" title="View all posts in Transport &amp; Avaition">Transport &amp; Avaition</a>
                                                                                                                </div>  
                                                                                                            </div>    
                                                                                                        </div>   
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="item-columns all financial-analysis ">
                                                                                                    <div class="portfolio-block portfolio-v1 isotope-item grid">      
                                                                                                        <div class="portfolio-content">
                                                                                                            <div class="images">
                                                                                                                <a class="link-image-content" href="http://themesgavias.com/wp/winnex/?portfolio=mobile-weather-app">
                                                                                                                    <img width="500" height="375" src="http://themesgavias.com/wp/winnex/wp-content/uploads/2017/03/portfolio-4-500x375.jpg" class="attachment-winnex_medium size-winnex_medium wp-post-image" alt="" />            </a>
                                                                                                                <a class="link" href="http://themesgavias.com/wp/winnex/?portfolio=mobile-weather-app">
                                                                                                                    <i class="gv-icon-809">
                                                                                                                    </i>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="content-inner">
                                                                                                                <div class="title">
                                                                                                                    <a href="http://themesgavias.com/wp/winnex/?portfolio=mobile-weather-app">Mobile Weather App</a>
                                                                                                                </div> 
                                                                                                                <div class="category">
                                                                                                                    <a href="http://themesgavias.com/wp/winnex/?category_portfolio=financial-analysis" title="View all posts in Financial Analysis">Financial Analysis</a>
                                                                                                                </div>  
                                                                                                            </div>    
                                                                                                        </div>   
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div> 

                                                                                    </div>
                                                                                    <div class="wpb_text_column wpb_content_element  vc_custom_1542077582875 text-center margin-bottom-0" >
                                                                                        <div class="wpb_wrapper">
                                                                                            <div class="clearfix text-medium text-black margin-top-30">
                                                                                                <p>We’re your finance and business experts. Contact us <strong>today and help</strong> is on the way!</p>
                                                                                                <p>
                                                                                                    <a class="btn-theme" href="#">Schedule a Service</a>
                                                                                                </p>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>