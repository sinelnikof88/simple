<div role="form" class="wpcf7" id="wpcf7-f3500-p36-o1" lang="en-US" dir="ltr">
    <div class="screen-reader-response">
    </div>
    <form action="/Callback" method="post" class="wpcf7-form" novalidate="novalidate">
        <div style="display: none;">
            <input type="hidden" name="_wpcf7" value="3500" />
            <input type="hidden" name="_wpcf7_version" value="5.0.5" />
            <input type="hidden" name="_wpcf7_locale" value="en_US" />
            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3500-p36-o1" />
            <input type="hidden" name="_wpcf7_container_post" value="36" />
        </div>
        <div class="request-call-back-form">
<!--            <div class="form-item">
                <span class="wpcf7-form-control-wrap your-name">
                    <input type="text" name="Callback[subject]" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Заголовок *" />
                </span>
            </div>-->
            <div class="form-item">
                <span class="wpcf7-form-control-wrap your-name">
                    <input type="text" name="Callback[name]" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Как вас зовут *" />
                </span>
            </div>
            <div class="form-item">
                <span class="wpcf7-form-control-wrap your-phone">
                    <input type="tel" name="Callback[phone]" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Ваш телефон*" />
                </span> 
            </div>
<!--            <div class="form-item">
                <span class="wpcf7-form-control-wrap your-email">
                    <input type="email" name="Callback[email]" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Ваш Email*" />
                </span> 
            </div>-->
            <div class="form-item">
                <span class="wpcf7-form-control-wrap your-email">
                    <input type="email" name="Callback[text]" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Пожелания" />
                </span> 
            </div>
            <div class="form-item submit">
                <input type="submit" value="Оставить заявку" class="wpcf7-form-control wpcf7-submit" />
            </div>
        </div>
        <div class="wpcf7-response-output wpcf7-display-none">
        </div>
    </form>
</div>