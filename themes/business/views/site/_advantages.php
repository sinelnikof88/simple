 

<div class="vc_wpb_row_inner  ">
    <div class="vc_row wpb_row vc_row-fluid row-container vc_row-o-equal-height vc_row-flex">
        <div class="container">
            <div class="row vc_row vc_row-o-equal-height vc_row-flex wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-8 vc_col-lg-8 vc_col-md-8">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget gsc-icon-box text-dark top-left  margin-bottom-2" >
                                                <div class="highlight-icon">
                                                    <span class="icon-image">
                                                        <img src="<?= @$model->_value['image1'] ?>"/> </span>
                                                </div>

                                                <div class="highlight_content">
                                                    <div class="title">
                                                        <?= @$model->_value['title1'] ?>         </div>
                                                    <div class="desc">  <?= @$model->_value['text1'] ?> </div>

                                                </div>

                                            </div> 


                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget gsc-icon-box text-dark top-left  margin-bottom-2" >
                                                <div class="highlight-icon">
                                                    <span class="icon-image">
                                                        <img src="<?= @$model->_value['image2'] ?>"/> </span>
                                                </div>

                                                <div class="highlight_content">
                                                    <div class="title">
                                                        <?= @$model->_value['title2'] ?>         </div>
                                                    <div class="desc">  <?= @$model->_value['text2'] ?> </div>

                                                </div>

                                            </div> 


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget gsc-icon-box text-dark top-left  margin-bottom-1" >
                                                <div class="highlight-icon">
                                                    <span class="icon-image">
                                                        <img src="<?= @$model->_value['image3'] ?>"/> </span>
                                                </div>

                                                <div class="highlight_content">
                                                    <div class="title">
                                                        <?= @$model->_value['title3'] ?>          </a>         </div>
                                                    <div class="desc">  <?= @$model->_value['text3'] ?> </div>

                                                </div>

                                            </div> 


                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="widget gsc-icon-box text-dark top-left  margin-bottom-1" >
                                                <div class="highlight-icon">
                                                    <span class="icon-image">
                                                        <img src="<?= @$model->_value['image4'] ?>"/> </span>
                                                </div>

                                                <div class="highlight_content">
                                                    <div class="title">
                                                        <?= @$model->_value['title4'] ?>        </div>
                                                    <div class="desc">  <?= @$model->_value['text4'] ?> </div>

                                                </div>

                                            </div> 


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column-border wpb_column vc_column_container vc_col-sm-4 vc_col-lg-4 vc_col-md-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="widget gsc-heading  align-left style-2 text-dark">
                                <h2 class="title">
                                    <span>Заявка на займ </span>
                                </h2>  
                                <div class="line">
                                    <span>
                                    </span>
                                </div>
                                <div class="title-desc">Заполните и отправьте форму, и мы свяжемся с вами в течение 5 минут.</div>
                            </div>
                            <?php $this->renderPartial('_form') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>