<?php

/**
 * This is the model class for table "menu".
 *
 * The followings are the available columns in table 'menu':
 * @property integer $id
 * @property integer $parent
 * @property string $name
 * @property string $url
 * @property integer $sort
 */
class Menu extends CActiveRecord {

    public $childs = [];

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'menu';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('parent, sort', 'numerical', 'integerOnly' => true),
            array('name, url', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, parent, name, url, sort', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'parent' => 'Parent',
            'name' => 'Name',
            'url' => 'Url',
            'sort' => 'Sort',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('parent', $this->parent);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('sort', $this->sort);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Menu the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function findList() {
        $data = $this->model()->findAll();
        return $this->buildTreeLinks($data);
    }

    public function buildTree(&$data, $rootID = 0) {
        $tree = array();
        foreach ($data as $id => $node) {
            if ($node->parent == $rootID) {
                $node->childs = $this->buildTree($data, $node->id);
                $tree[] = $node;
            }
        }
        return $tree;
    }

    public function buildTreeLinks(&$data, $rootID = 0) {
        $tree = [];
        foreach ($data as $id => $node) {
            if ($node->parent == $rootID) {
                $node->childs = $this->buildTreeLinks($data, $node->id);
                $ch = !empty($node->childs) ? ['items' => $node->childs] : [];
                $tree[$node->id] = array_merge([
                    'label' => $node->name,
                    'url' => $node->url
                        ], $ch);
            }
        }
        return $tree;
    }

}
