<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class ConfigForm extends CFormModel {

    public $title, $name, $description, $icon, $keywords;
    public $phone;
    public $logo;
    public $image;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // username and password are required
            array('name', 'safe'),
            array('title', 'safe'),
            array('description', 'safe'),
            array('icon', 'safe'),
            array('phone', 'safe'),
            array('logo', 'safe'),
            array('image', 'safe'),
            array('keywords', 'safe'),
                // rememberMe needs to be a boolean
                // password needs to be authenticated
        );
    }

    public function load() {
        $this->attributes = (array) yii::app()->config;
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'name' => 'Название сайта',
            'title' => 'Титл',
            'description' => 'Дескрипшон',
            'icon' => 'Иконка',
            'phone' => 'контакты-телефон',
            'logo' => 'Логотип',
            'image' => 'Изображение для сео',
            'keywords' => 'Ключевые слова'
        );
    }

    protected $localFile = '';

    public function save() {
        $this->localFile = yii::getPathOfAlias('app.data.config') . '.data';
        file_put_contents($this->localFile, serialize($this->attributes));
        return true;
    }

}
