<?php

/**
 * This is the model class for table "Pages".
 *
 * The followings are the available columns in table 'Pages':
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $image
 * @property string $text
 * @property integer $is_active
 * @property integer $group_id
 */
class Pages extends CriteriaActiveRecord implements Iswiping {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'pages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('is_active', 'numerical', 'integerOnly' => true),
            array('name, title, image, text', 'safe'),
            array('h1, description,slug, keywords, group_id', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, title, image, text, is_active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'pageGroup' => [self::BELONGS_TO, 'PagesGroups', 'group_id']
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Title',
            'image' => 'Image',
            'text' => 'Text',
            'is_active' => 'Is Active',
            'group_id' => 'Группа страниц',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('description', $this->is_active);
        $criteria->compare('title', $this->is_active);
        $criteria->compare('h1', $this->is_active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id desc',
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Pages the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function makeSlug($title) {
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $title);
        $clean = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $clean);
        $clean = strtolower(trim($clean));
        $clean = preg_replace("/[\/_| -]+/", $this->replace_space, $clean);

        return $clean;
    }

    public $replace_space = '-';

    public function getLink() {
        return yii::app()->createAbsoluteUrl('/', ['content' => $this->slug]);
    }

    public function getSwiperContent() {
        return $this->_content;
    }

    protected $_content;

    public function setSwiperContent($content) {
        $this->_content = $content;
    }

}
