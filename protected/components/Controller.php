<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    public $adminMenu = [];

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function beforeAction($action) {


        $this->adminMenu = [
            ['label' => 'Главная', 'url' => ['/site/index']],
            ['label' => 'Админка', 'url' => ['/adm']],
            ['label' => 'Слайдер', 'url' => ['/adm/slider/admin']],
            ['label' => 'Меню', 'url' => ['/adm/SortedMenu/admin']],
            ['label' => 'Обратная связь', 'url' => ['/adm/Callback/admin']],
            ['label' => 'Калькулятор', 'url' => ['/adm/Calculator/admin']],
            ['label' => 'Контент',
                'items' => [
                    ['label' => 'Сотрудники', 'url' => ['/adm/Employees/admin']],
                    ['label' => 'Новости', 'url' => ['/adm/News/admin']],
                    ['label' => 'Группы страниц', 'url' => ['/adm/PagesGroups/admin']],
                    ['label' => 'Страницы', 'url' => ['/adm/pages/admin']],
                ]],
            ['label' => 'Блоки', 'url' => ['/adm/block'],
                'items' => [
                    ['label' => 'Список блоков', 'url' => ['/adm/block']],
                    ['label' => 'Добавить блок', 'url' => ['/adm/block'],
                        'items' => [
                            ['label' => 'Преимущества с обратой связью', 'url' => yii::app()->createAbsoluteUrl('/adm/', ['block' => 'create', 'theme' => '_advantages'])],
                            ['label' => 'Простые приемущества', 'url' => yii::app()->createAbsoluteUrl('/adm/', ['block' => 'create', 'theme' => '_advantages_simple'])],
                            ['label' => 'Текст', 'url' => yii::app()->createAbsoluteUrl('/adm/', ['block' => 'create', 'theme' => '_text'])],
                            ['label' => 'Сотрудники', 'url' => yii::app()->createAbsoluteUrl('/adm/', ['block' => 'create', 'theme' => '_employees'])],
                            ['label' => 'Счетчики', 'url' => yii::app()->createAbsoluteUrl('/adm/', ['block' => 'create', 'theme' => '_counters'])],
                        ]
                    ],]
            ],
            ['label' => 'Вход', 'url' => ['/site/login'], 'visible' => Yii::app()->user->isGuest],
        ];



        $this->pageTitle = yii::app()->config->title;
        Yii::app()->clientScript->registerMetaTag(yii::app()->config->keywords, 'keywords');
        Yii::app()->clientScript->registerMetaTag(yii::app()->config->description, 'description');

        return parent:: beforeAction($action);
    }

}
