<?php

/**
 * Вспомогательный класс для картинок
 */
class ImageHelper {

    /**
     * размер 50х50
     */
    public static function sizeMini($param) {
        return'/site/resized/50x50/' . $param;
    }

    /**
     * размер 100х100
     */
    public static function sizeSmall($param) {
        return'/site/resized/100x100/' . $param;
    }

    /**
     * размер 300х300
     */
    public static function sizeMedium($param) {
        return'/site/resized/300x300/' . $param;
    }
    /**
     * размер 300х300
     */
    public static function sizeBig($param) {
        return'/site/resized/600x600/' . $param;
    }

}
