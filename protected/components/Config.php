<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Config
 *
 * @author i.gonnyh
 */
class Config extends CComponent {

    public $title, $name, $description, $keywords, $icon;
    public $logo;
    public $logoText;
    public $image;
    public $phone;
    public $siteName;
    protected $localFile = '';

    public function init() {
        $this->localFile = yii::getPathOfAlias('app.data.config') . '.data';
        if (!file_exists($this->localFile)) {
            file_put_contents($this->localFile, serialize([]));
        }
        $file = unserialize(file_get_contents($this->localFile));
        $this->name = $file['name'] ?? '';
        $this->title = $file['title'] ?? '';
        $this->description = $file['description'] ?? '';
        $this->icon = $file['icon'] ?? '';
        $this->logo = $file['logo'] ?? '';
        $this->image = $file['image'] ?? '';
        $this->keywords = $file['keywords'] ?? '';
        $this->phone = $file['phone'] ?? '';
    }

}
