<?php

class CriteriaActiveRecord extends CActiveRecord {

    public function order($order) {
	$this->getDbCriteria()->mergeWith(array(
	    'order' => $order,
	));

	return $this;
    }

    public function index($index) {
	$this->getDbCriteria()->mergeWith(array(
	    'index' => $index,
	));

	return $this;
    }

    public function __call($name, $parameters) {
	if (substr($name, 0, 4) == 'like') {

	    if (method_exists($this, $name)) {
		return parent::__call($name, $parameters);
	    }

	    if (empty($parameters)) {
		return parent::__call($name, $parameters);
	    }
	    return $this->like(
			    substr($name, 4), $parameters[0], isset($parameters[1]) ? $parameters[1] : null, isset($parameters[2]) ? $parameters[2] : true);
	}

	if (substr($name, 0, 2) == 'by') {

	    if (method_exists($this, $name)) {
		return parent::__call($name, $parameters);
	    }

	    if (empty($parameters)) {
		return parent::__call($name, $parameters);
	    }
	    return $this->by(
			    substr($name, 2), $parameters[0], isset($parameters[1]) ? $parameters[1] : null, isset($parameters[2]) ? $parameters[2] : true);
	}
	if (substr($name, 0, 3) == 'not') {

	    if (method_exists($this, $name)) {
		return parent::__call($name, $parameters);
	    }

	    if (empty($parameters)) {
		return parent::__call($name, $parameters);
	    }

	    return $this->not(substr($name, 3), $parameters[0], isset($parameters[1]) ? $parameters[1] : null);
	}

	return parent::__call($name, $parameters);
    }

    public function like($attribute, $value, $table = null, $withTable = true) {
	return $this->by($attribute, $value, $table, $withTable, ' LIKE ');
    }

    // меньше либо ровно
    public function less($attribute, $value, $table = null, $withTable = true) {
	return $this->by($attribute, $value, $table, $withTable, ' <= ');
    }

    // больше либо ровно

    public function more($attribute, $value, $table = null, $withTable = true) {
	return $this->by($attribute, $value, $table, $withTable, ' >= ');
    }

    public function not($attribute, $value, $table = null, $withTable = true, $operator = '=') {
	return $this->by($attribute, $value, $table, $withTable, '<>');
    }

    public function between($column, $min, $max, $table = null, $withTable = true)
    {    
        $criteria = new CDbCriteria();

        if (!is_null($table) && $withTable) {
	    $criteria->with = array($table);
	}
        if (is_null($table)) {
	    $table = $this->getTableAlias();
	}
 	$criteria->addCondition($table . '.' .strtolower($column) . ' BETWEEN "' . $min . '" AND "' . $max . '"');
	$this->getDbCriteria()->mergeWith($criteria);
	return $this;
    }

    public function by($attribute, $value, $table = null, $withTable = true, $operator = '=') {
	if (is_null($value)) {
	    return $this;
	}
	$criteria = new CDbCriteria();

	if (!is_null($table) && $withTable) {
	    $criteria->with = array($table);
	}

	if (is_null($table)) {
	    $table = $this->getTableAlias();
	}

	if (is_array($value)) {
	    if ($operator == '=') {
		$criteria->addInCondition($table . '.' . strtolower($attribute), $value);
	    } else {
		$criteria->addNotInCondition($table . '.' . strtolower($attribute), $value);
	    }
	} elseif ($value == 'null' || $value == 'not null') {
	    $criteria->addCondition($table . '.' . strtolower($attribute) . ' IS ' . $value);
	} else {
	    $criteria->addCondition($table . '.' . strtolower($attribute) . ' ' . $operator . ' :' . $this->getTableAlias() . $attribute);
	    $criteria->params = array($this->getTableAlias() . $attribute => $value);
	}

	$this->getDbCriteria()->mergeWith($criteria);
	return $this;
    }

    public function group($group) {

	$this->getDbCriteria()->mergeWith(array(
	    'group' => $group,
	));

	return $this;
    }

    public function select($select) {

	$this->getDbCriteria()->mergeWith(array(
	    'select' => $select,
	));

	return $this;
    }

    public function together() {

	$this->getDbCriteria()->mergeWith(array(
	    'together' => true,
	));

	return $this;
    }

    public function join($join) {

	$this->getDbCriteria()->mergeWith(array(
	    'join' => $join,
	));

	return $this;
    }

    public function in($column, $values, $operator = 'AND') {
	$criteria = new CDbCriteria;
	$criteria->addInCondition($column, $values, $operator);
	$this->getDbCriteria()->mergeWith($criteria);

	return $this;
    }

    public function sum($column) {
	$this->getDbCriteria()->mergeWith(array(
	    'select' => '*,SUM(`t`.' . $column . ') as ' . $column,
	));

	return $this;
    }

    public function limit($limit) {

	$this->getDbCriteria()->mergeWith(array(
	    'limit' => $limit,
	));

	return $this;
    }

    public function condition($condition, $params = array()) {

	$this->getDbCriteria()->mergeWith(array(
	    'condition' => $condition,
	    'params' => $params,
	));

	return $this;
    }

    public function setParam($name, $value) {
	$this->getDbCriteria()->params[$name] = $value;
	return $this;
    }

    public function ignore() {
	print_r($this);
	exit;
    }

}

?>
