<?php
$this->breadcrumbs=array(
	'Sorted Menus',
);

$this->menu=array(
	array('label'=>'Create SortedMenu','url'=>array('create')),
	array('label'=>'Manage SortedMenu','url'=>array('admin')),
);
?>

<h1>Sorted Menus</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
