<?php
$this->breadcrumbs=array(
	'Sorted Menus'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SortedMenu','url'=>array('index')),
	array('label'=>'Manage SortedMenu','url'=>array('admin')),
);
?>

<h1>Create SortedMenu</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>