<?php
$this->breadcrumbs=array(
	'Sorted Menus'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List SortedMenu','url'=>array('index')),
	array('label'=>'Create SortedMenu','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sorted-menu-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление меню</h1>

 
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'sorted-menu-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
 		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
