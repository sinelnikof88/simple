<?php
$this->breadcrumbs=array(
	'Sorted Menus'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SortedMenu','url'=>array('index')),
	array('label'=>'Create SortedMenu','url'=>array('create')),
	array('label'=>'View SortedMenu','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage SortedMenu','url'=>array('admin')),
);
?>

<h1>Update SortedMenu <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>


<button id="appendnestable">Добавить пункт</button>


<div class="dd" id="nestable3">
    <ol class="dd-list outer">   
        <?php $this->renderPartial('item', ['data' => $model->items]) ?>
    </ol>
</div>


<script>
    function initNest() {
        $('#nestable3').nestable();
         $(document).on('change','#nestable3', function (e) {
            var list = e.length ? e : $(e.target), output = list.data('output');
            if (window.JSON) {
                d = {'SortedMenu': {'data': window.JSON.stringify(list.nestable('serialize'))}};
                $.post('/adm/sortedMenu/update/id/<?= $model->id ?>', d, function () {
//                toastr.success('Сохарненно', '', {timeOut: 5000});
                });
            } else {
                output.val('JSON browser support required for this demo.');
            }
        })

        $('#nestable3').on('click', '.delete', function (e) {
            e.preventDefault();
            $(this).parent().parent().parent().parent().parent().remove();
        })
        $('#nestable3').on('click', '.update', function (e) {
            e.preventDefault();
            $el = $(this).parent().parent().parent();
            $data = {label: $el.data('label'), url: $el.data('url')}
            console.log($data);
        })
        $(document).on('submit', 'form', function (e) {
            e.preventDefault();
            $('#nestable3').change();
        })
        $('.dd3-content  input').on('keyup', function (e) {
            var Mclass = $(this).data('master');
            master = '.master-' + Mclass;
            attrName = $(this).attr('name');
            $(master).attr('data-' + attrName, $(this).val());
//        var parent = $(this).parent('li.dd-item.dd3-item');
//        parent.attr('data-name', $(this).val());
//        parent.addClass($(this).val());
            console.log(Mclass);
        })
    }



    $('#appendnestable').click(function () {
        $.get('/adm/sortedMenu/createItem', function (data) {
            $('ol.outer').append(data);
//$('#nestable3').nestable('destroy');
//// Unbind all event handlers!
//$('#nestable3').unbind();

// Re-initialize the sortable feature
            initNest();
        })
    });
    initNest();
</script>