<?php $item['id'] = uniqid()?>

<li  class=" dd-item dd3-item master-<?= @$item['id'] ?>" data-id="<?= @$item['id'] ?>" data-label="<?= @$item['label'] ?>" data-url="<?= @$item['url'] ?>">
    <div class="dd-handle dd3-handle">Drag</div>
    <div class="dd3-content  ">
        <div class="row-fluid">
            <div class="span10"> 
                <form class="form-horizontal" id='<?= @$item['id'] ?>' style="float: left;     width: 95%;   display: inline-block" class='itemForm  '>

                    <input data-master=<?= @$item['id'] ?>  class="span5" value='<?= @$item['label'] ?>' name=label placeholder=label>

                    <input data-master=<?= @$item['id'] ?>  class="span5"   value='<?= @$item['url'] ?>' name=url  placeholder=url>

                    <button class="btn btn-primary" type="submit" name="yt0">Save</button>        </form>
            </div>
            <div class="span2" ><span  class=" "  style="float: right" class="button-column">
                    <a class="delete">
                        <i class="icon-trash"></i>
                    </a>
                </span>  </div>
        </div>





    </div>
    <?php if (!empty($item['children'])): ?>
        <ol class="dd-list">
            <?php $this->renderPartial('item', ['data' => $item['children']]) ?>
        </ol> 
    <?php endif; ?>
</li>