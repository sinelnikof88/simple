<?php
$this->breadcrumbs = array(
    'Sliders' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Добавить', 'url' => array('create')),
);
?>

<h1>Управление слайдером</h1>



<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'slider-grid',
    'rowHtmlOptionsExpression' => 'array("id"=>$data->id)',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'title',
        ['name' => 'image', 'type' => 'raw', 'value' => function ($data) {
                return CHtml::image(ImageHelper::sizeMini($data->image));
            }], 'sort',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
));
?>

<?php
Yii::app()->clientScript->registerScriptFile('http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js');

$str_js = "
		function innert(){
                
var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};
			
        $('#slider-grid table.items tbody').sortable({
                forcePlaceholderSize: true,
                forceHelperSize: true,
                items: 'tr',
                update : function () {
                items = $(this).sortable('toArray', {attribute: 'id'}); // switch from serialize to toArray
                 $.ajax({
                    'url': '" . $this->createUrl('/adm/slider/sort') . "',
                    'type': 'post',
                    //'data': serial,
                                data: {items: items}, // sending the array to controller

                    'success': function(data){
                     $.fn.yiiGridView.update('slider-grid');
                        setTimeout(function(){
                        innert();

                        },300);         
                    },
                    'error': function(request, status, error){
                            alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                    }
                    });
                },
                         
			helper: fixHelper
		}).disableSelection();
                };
                innert();
	";

Yii::app()->clientScript->registerScript('slider-grid', $str_js);
?>