<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'slider-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textAreaRow($model, 'title', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php
$this->widget('ext.choiseImage.ChoiseImage', array(
    'model' => $model,
    'attribute' => 'image',
));
?> 
<?php echo $form->textFieldRow($model, 'sort', array('class' => 'span5')); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
