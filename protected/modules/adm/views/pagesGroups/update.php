<?php
$this->breadcrumbs=array(
	'Pages Groups'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PagesGroups','url'=>array('index')),
	array('label'=>'Create PagesGroups','url'=>array('create')),
	array('label'=>'View PagesGroups','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PagesGroups','url'=>array('admin')),
);
?>

<h1>Update PagesGroups <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>