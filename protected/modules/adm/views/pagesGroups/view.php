<?php
$this->breadcrumbs=array(
	'Pages Groups'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PagesGroups','url'=>array('index')),
	array('label'=>'Create PagesGroups','url'=>array('create')),
	array('label'=>'Update PagesGroups','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete PagesGroups','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PagesGroups','url'=>array('admin')),
);
?>

<h1>View PagesGroups #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
		'image',
	),
)); ?>
