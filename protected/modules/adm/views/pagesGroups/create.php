<?php
$this->breadcrumbs=array(
	'Pages Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PagesGroups','url'=>array('index')),
	array('label'=>'Manage PagesGroups','url'=>array('admin')),
);
?>

<h1>Create PagesGroups</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>