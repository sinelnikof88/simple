<?php
$this->breadcrumbs=array(
	'Pages Groups',
);

$this->menu=array(
	array('label'=>'Create PagesGroups','url'=>array('create')),
	array('label'=>'Manage PagesGroups','url'=>array('admin')),
);
?>

<h1>Pages Groups</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
