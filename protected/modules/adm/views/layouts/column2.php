<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
<div class="row-fluid"> 

    <div class="<?= !empty($this->menu) ? "span10" : "span10" ?>">
        <div id="content">

            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('bootstrap.widgets.TbBreadcrumbs', [
                    'links' => $this->breadcrumbs,
                ]);
                ?><!-- breadcrumbs -->
            <?php endif ?>

            <div class ="mainPageContent">
                <?php echo $content; ?>
            </div><!-- content -->
        </div>
    </div>
    <div class="<?= !empty($this->menu) ? "span2" : "hiden" ?>">

        <div id="sidebar">
            <div id="siteBarWrapper">
                <?php
                $this->beginWidget('zii.widgets.CPortlet', [
                    'title' => 'Меню',
                    'htmlOptions' => ['class' => 'portlet card']
                ]);
                $this->widget('bootstrap.widgets.TbMenu', array(
                    'type' => 'list', // '', 'tabs', 'pills' (or 'list')
                    'stacked' => false, // whether this is a stacked menu
                    'items' => $this->menu
                ));
                $this->endWidget();
                ?>    
            </div>
        </div><!-- sidebar -->
    </div> 
</div>
<?php $this->endContent(); ?>