<?php
$this->breadcrumbs=array(
	'Callbacks'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Callback','url'=>array('index')),
	array('label'=>'Create Callback','url'=>array('create')),
	array('label'=>'Update Callback','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Callback','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Callback','url'=>array('admin')),
);
?>

<h1>View Callback #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'subject',
		'name',
		'phone',
		'email',
		'text',
	),
)); ?>
