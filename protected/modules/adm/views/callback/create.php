<?php
$this->breadcrumbs=array(
	'Callbacks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Callback','url'=>array('index')),
	array('label'=>'Manage Callback','url'=>array('admin')),
);
?>

<h1>Create Callback</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>