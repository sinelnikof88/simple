<?php
$this->breadcrumbs=array(
	'Callbacks'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Callback','url'=>array('index')),
	array('label'=>'Create Callback','url'=>array('create')),
	array('label'=>'View Callback','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Callback','url'=>array('admin')),
);
?>

<h1>Update Callback <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>