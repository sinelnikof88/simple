<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'pages-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model, 'name', array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>
        <?php echo $form->textFieldRow($model, 'title', array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>
        <?php echo $form->textFieldRow($model, 'slug', array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>
        <?php echo $form->textFieldRow($model, 'h1', array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>
        <?php echo $form->textFieldRow($model, 'keywords', array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>
        <?php echo $form->textFieldRow($model, 'description', array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>
    </div>
    <div class="span6">
        <?php echo $form->dropDownListRow($model, 'group_id', chtml::listData(PagesGroups::model()->findAll(), 'id', 'name'), array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>
        <?php
        $this->widget('ext.choiseImage.ChoiseImage', array(
            'model' => $model,
            'attribute' => 'image',
        ));
        ?>      
    </div>
</div>
<h2>Контент страницы</h2>
<div class="row-fluid">
    <div class="span12">

        <?php
        $this->widget('ext.wysibb.WysiBb', array(
            'model' => $model,
            'attribute' => 'text',
//            'options' => ['buttons' => "bold,italic,underline,fontcolor,fontsize,fontfamily"],
        ));
        ?>  
    </div>
</div>

<?php echo $form->checkBoxRow($model, 'is_active', array('class' => ' ')); ?>


<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>

<?php if ($model->isNewRecord): ?>
    <script>
        $(document).on('keyup', '#Pages_name', function () {
            text = ($(this).val());
            $('#Pages_title').val(text);
            $('#Pages_slug').val(translite(text));
            $('#Pages_h1').val((text));
            $('#Pages_keywords').val((text));
            $('#Pages_description').val((text));

        })



        function translite(str) {
            var arr = {'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ж': 'g', 'з': 'z', 'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'ы': 'i', 'э': 'e', 'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ж': 'G', 'З': 'Z', 'И': 'I', 'Й': 'Y', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O', 'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Ы': 'I', 'Э': 'E', 'ё': 'yo', 'х': 'h', 'ц': 'ts', 'ч': 'ch', 'ш': 'sh', 'щ': 'shch', 'ъ': '', 'ь': '', 'ю': 'yu', 'я': 'ya', 'Ё': 'YO', 'Х': 'H', 'Ц': 'TS', 'Ч': 'CH', 'Ш': 'SH', 'Щ': 'SHCH', 'Ъ': '', 'Ь': '',
                'Ю': 'YU', 'Я': 'YA'};
            var replacer = function (a) {
                return arr[a] || a
            };
            return str.replace(/ /g, "_").replace(/[А-яёЁ]/g, replacer)
        }
    //    alert(translite('алексашка'))

    </script>
<?php endif; ?>