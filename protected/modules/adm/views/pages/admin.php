<?php
$this->breadcrumbs = array(
    'Pages' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Pages', 'url' => array('index')),
    array('label' => 'Create Pages', 'url' => array('create')),
);
?>

<h1>Управление страницами</h1>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'pages-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        ['name' => 'image', 'type' => 'raw', 'value' => function ($data) {
                return CHtml::image(ImageHelper::sizeMini($data->image));
            }],
        'name',
        'title',
        'slug',
        'link',
        'is_active',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
));
?>
