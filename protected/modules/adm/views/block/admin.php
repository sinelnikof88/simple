<?php
$this->breadcrumbs = array(
    'Blocks' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Block', 'url' => array('index')),
    array('label' => 'преимущества с обратой связью', 'url' => array('create', 'theme' => '_advantages')),
    array('label' => 'Простые приемущества', 'url' => array('create', 'theme' => '_advantages_simple')),
    array('label' => 'текст', 'url' => array('create', 'theme' => '_text')),
    array('label' => 'сотрудники', 'url' => array('create', 'theme' => '_employees')),
    array('label' => 'счетчики', 'url' => array('create', 'theme' => '_counters')),
    array('label' => 'Create Block', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('block-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Blocks</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button btn')); ?>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'block-grid',
    'rowHtmlOptionsExpression' => 'array("id"=>$data->id)',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'typeName',
        'name',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'buttons' => ['update' => array
                    (
                    'url' => 'Yii::app()->createUrl("/adm/block/update", array("id"=>$data->id,"theme"=>$data->type))',
                ),]
        ),
    ),
));
?>

<?php
Yii::app()->clientScript->registerScriptFile('http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js');

$str_js = "
		function innert(){
                
var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};
			
        $('#block-grid table.items tbody').sortable({
                forcePlaceholderSize: true,
                forceHelperSize: true,
                items: 'tr',
                update : function () {
                items = $(this).sortable('toArray', {attribute: 'id'}); // switch from serialize to toArray
                 $.ajax({
                    'url': '" . $this->createUrl('/adm/block/sort') . "',
                    'type': 'post',
                    //'data': serial,
                                data: {items: items}, // sending the array to controller

                    'success': function(data){
                     $.fn.yiiGridView.update('block-grid');
                        setTimeout(function(){
                        innert();

                        },300);         
                    },
                    'error': function(request, status, error){
                            alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                    }
                    });
                },
                         
			helper: fixHelper
		}).disableSelection();
                };
                innert();
	";

Yii::app()->clientScript->registerScript('block-grid', $str_js);
?>