<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'block-1-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->hiddenField($model, 'type', array('rows' => 1, 'value'=>$theme, 'cols' => 50, 'class' => 'span8')); ?>
<?php echo $form->textAreaRow($model, 'name', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->textAreaRow($model, '_value[title]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
<?php echo $form->textAreaRow($model, '_value[description]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>

<div class=" row-fluid">
    <div class="span4">
        <?php
        $this->widget('ext.choiseImage.ChoiseImage', array(
            'model' => $model,
            'attribute' => '_value[image1]',
        ));
        ?>        
        <?php echo $form->textAreaRow($model, '_value[title1]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php
        $this->widget('ext.wysibb.WysiBb', array(
            'model' => $model,
            'attribute' => '_value[text1]',
            'options' => ['buttons' => "bold,italic,underline,fontcolor,fontsize,fontfamily"],
        ));
        ?> 
    </div>
    <div class="span4">
        <?php
        $this->widget('ext.choiseImage.ChoiseImage', array(
            'model' => $model,
            'attribute' => '_value[image2]',
        ));
        ?>       
        <?php echo $form->textAreaRow($model, '_value[title2]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php
        $this->widget('ext.wysibb.WysiBb', array(
            'model' => $model,
            'attribute' => '_value[text2]',
            'options' => ['buttons' => "bold,italic,underline,fontcolor,fontsize,fontfamily"],
        ));
        ?> 
    </div>
    <div class="span4">
        <?php
        $this->widget('ext.choiseImage.ChoiseImage', array(
            'model' => $model,
            'attribute' => '_value[image3]',
        ));
        ?>         
        <?php echo $form->textAreaRow($model, '_value[title3]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php
        $this->widget('ext.wysibb.WysiBb', array(
            'model' => $model,
            'attribute' => '_value[text3]',
            'options' => ['buttons' => "bold,italic,underline,fontcolor,fontsize,fontfamily"],
        ));
        ?> 
    </div>
</div>
<hr>
<div class=" row-fluid">
    <div class="span4">
        <?php
        $this->widget('ext.choiseImage.ChoiseImage', array(
            'model' => $model,
            'attribute' => '_value[image4]',
        ));
        ?>        
        <?php echo $form->textAreaRow($model, '_value[title4]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php
        $this->widget('ext.wysibb.WysiBb', array(
            'model' => $model,
            'attribute' => '_value[text4]',
            'options' => ['buttons' => "bold,italic,underline,fontcolor,fontsize,fontfamily"],
        ));
        ?> 
    </div>
    <div class="span4">
        <?php
        $this->widget('ext.choiseImage.ChoiseImage', array(
            'model' => $model,
            'attribute' => '_value[image5]',
        ));
        ?>        
        <?php echo $form->textAreaRow($model, '_value[title5]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php
        $this->widget('ext.wysibb.WysiBb', array(
            'model' => $model,
            'attribute' => '_value[text5]',
            'options' => ['buttons' => "bold,italic,underline,fontcolor,fontsize,fontfamily"],
        ));
        ?> 
    </div>
    <div class="span4">
        <?php
        $this->widget('ext.choiseImage.ChoiseImage', array(
            'model' => $model,
            'attribute' => '_value[image6]',
        ));
        ?>         
        <?php echo $form->textAreaRow($model, '_value[title6]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php
        $this->widget('ext.wysibb.WysiBb', array(
            'model' => $model,
            'attribute' => '_value[text6]',
            'options' => ['buttons' => "bold,italic,underline,fontcolor,fontsize,fontfamily"],
        ));
        ?> 
    </div>
</div>


<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
