<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'block-1-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->hiddenField($model, 'type', array('rows' => 1, 'value' => $theme, 'cols' => 50, 'class' => 'span8')); ?>
<?php echo $form->textAreaRow($model, 'name', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->textAreaRow($model, '_value[title]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
<?php echo $form->textAreaRow($model, '_value[description]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>

<div class=" row-fluid">
    <div class="span4">
        <?php echo $form->textFieldRow($model, '_value[item1]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php echo $form->textFieldRow($model, '_value[item1-text]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php echo $form->textFieldRow($model, '_value[item1-symbol]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
    </div>
    <div class="span4">
        <?php echo $form->textFieldRow($model, '_value[item2]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php echo $form->textFieldRow($model, '_value[item2-text]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php echo $form->textFieldRow($model, '_value[item2-symbol]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
    </div>
    <div class="span4">
        <?php echo $form->textFieldRow($model, '_value[item3]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php echo $form->textFieldRow($model, '_value[item3-text]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
        <?php echo $form->textFieldRow($model, '_value[item3-symbol]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
    </div>
</div>


<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
