<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'block-1-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->hiddenField($model, 'type', array('rows' => 1, 'value' => $theme, 'cols' => 50, 'class' => 'span8')); ?>
<?php echo $form->textAreaRow($model, 'name', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->textAreaRow($model, '_value[title]', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
<?php
$this->widget('ext.wysibb.WysiBb', array(
    'model' => $model,
    'attribute' => '_value[text]',
    'options' => ['buttons' => "bold,italic,underline,fontcolor,fontsize,fontfamily"],
));
?> 
<?php
$this->widget('ext.choiseImage.ChoiseImage', array(
    'model' => $model,
    'attribute' => '_value[background]',
));
?>   

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>


<?php $this->endWidget(); ?>
