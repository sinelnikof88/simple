<?php
$this->breadcrumbs = array(
    'Menus' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Menu', 'url' => array('index')),
    array('label' => 'Create Menu', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('menu-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Menus</h1>


<div class="dd" id="nestable3">
    <ol class="dd-list">   
        <?php $this->renderPartial('item', ['data' => $model->findList()]) ?>
    </ol>
</div>


<script>
    $('#nestable3').nestable();
    $('#nestable3').on('change', function (e) {
        var list = e.length ? e : $(e.target), output = list.data('output');
        if (window.JSON) {
            d = {'Menu': {'data': window.JSON.stringify(list.nestable('serialize'))}};
            $.post('/admin/menu/update/id/<?= $model->id ?>', d, function () {
                toastr.success('Сохарненно', '', {timeOut: 5000});
            });
        } else {
            output.val('JSON browser support required for this demo.');
        }
    })

    $('#nestable3').on('click', '.delete', function (e) {
        e.preventDefault();
        $(this).parent().parent().parent().parent().parent().remove();
    })
    $('#nestable3').on('click', '.update', function (e) {
        e.preventDefault();
        $el = $(this).parent().parent().parent();
        $data = {label: $el.data('label'), url: $el.data('url')}
        console.log($data);
    })


    $(document).on('submit', '.itemForm', function (e) {
        e.preventDefault();
        var data = App.getFormData($(this));
        var id = $(this).attr('id');
        var elem = $(".dd-item[data-id='" + id + "']");
        elem.data('url', data.url)
        elem.data('label', data.label);
        $('#nestable3').change();
    })
</script>