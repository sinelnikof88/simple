<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'news-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textAreaRow($model, 'name', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
<?php echo $form->textAreaRow($model, 'title', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
<?php echo $form->textAreaRow($model, 'description', array('rows' => 1, 'cols' => 50, 'class' => 'span8')); ?>
 
<?php
$this->widget('ext.choiseImage.ChoiseImage', array(
    'model' => $model,
    'attribute' => 'image',
));
?> 
<?php
$this->widget('ext.wysibb.WysiBb', array(
    'model' => $model,
    'attribute' => 'text',
    'options' => ['buttons' => "bold,italic,underline,fontcolor,fontsize,fontfamily"],
));
?> 
    <?php echo $form->checkBoxRow($model, 'is_active', array('class' => ' ')); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
