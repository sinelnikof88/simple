<?php
/* @var $this DefaultController */

$this->breadcrumbs = array(
    $this->module->id,
);
?>
<h1><?php echo $this->uniqueId . '/' . $this->action->id; ?></h1>
<div class=" row-fluid">
    <div class="span12">
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>

    </div>
    <!--<div class="span6"></div>-->

</div>