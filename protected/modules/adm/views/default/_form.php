<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'pages-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>
<?php echo $form->errorSummary($model); ?>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textAreaRow($model, 'name', array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>
        <?php echo $form->textAreaRow($model, 'title', array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>
        <?php echo $form->textAreaRow($model, 'description', array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>
        <?php echo $form->textAreaRow($model, 'keywords', array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>
        <?php echo $form->textAreaRow($model, 'phone', array('rows' => 2, 'cols' => 50, 'class' => 'span12')); ?>

    </div>
    <div class="span6"><?php
        $this->widget('ext.choiseImage.ChoiseImage', array(
            'model' => $model,
            'attribute' => 'icon',
        ));
        ?>  
        <?php
        $this->widget('ext.choiseImage.ChoiseImage', array(
            'model' => $model,
            'attribute' => 'logo',
        ));
        ?> <?php
        $this->widget('ext.choiseImage.ChoiseImage', array(
            'model' => $model,
            'attribute' => 'image',
        ));
        ?> 
    </div>
</div>



<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
