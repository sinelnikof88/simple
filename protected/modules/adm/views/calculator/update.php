<?php
$this->breadcrumbs=array(
	'Calculators'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Calculator','url'=>array('index')),
	array('label'=>'Create Calculator','url'=>array('create')),
	array('label'=>'View Calculator','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Calculator','url'=>array('admin')),
);
?>

<h1>Update Calculator <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>