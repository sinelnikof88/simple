<?php
$this->breadcrumbs=array(
	'Calculators',
);

$this->menu=array(
	array('label'=>'Create Calculator','url'=>array('create')),
	array('label'=>'Manage Calculator','url'=>array('admin')),
);
?>

<h1>Calculators</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
