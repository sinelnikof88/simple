<?php
$this->breadcrumbs=array(
	'Calculators'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Calculator','url'=>array('index')),
	array('label'=>'Manage Calculator','url'=>array('admin')),
);
?>

<h1>Create Calculator</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>