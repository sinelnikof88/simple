<?php
$this->breadcrumbs=array(
	'Calculators'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Calculator','url'=>array('index')),
	array('label'=>'Create Calculator','url'=>array('create')),
	array('label'=>'Update Calculator','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Calculator','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Calculator','url'=>array('admin')),
);
?>

<h1>View Calculator #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'from',
		'to',
		'percent',
	),
)); ?>
