<?php

class DefaultController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public $layout = '/layouts/column2';

    public function actionIndex() {
        $this->menu = $this->adminMenu;

        $model = new ConfigForm();
        $model->load();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ConfigForm'])) {
            $model->attributes = $_POST['ConfigForm'];
            if ($model->save()) {
                yii::app()->user->setFlash('success', 'настройки сохраненны');
                $this->redirect('/adm/default/index', array(
                    'model' => $model,
                ));
            }
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

}
