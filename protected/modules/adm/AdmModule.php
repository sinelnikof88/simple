<?php

class AdmModule extends CWebModule {

    public $assetsDir;
    public $assetsUri;

    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'adm.models.*',
            'adm.components.*',
        ));


        $dir = dirname(__FILE__) . '/assets';
        $this->assetsDir = Yii::app()->assetManager->publish($dir, false, -1);
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($this->assetsDir . '/style.css?');
//        $cs->registerScriptFile($this->assetsDir . '/js/gdevse/moment.js', CClientScript::POS_END);
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }

}
