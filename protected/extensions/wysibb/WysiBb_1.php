<?php

class WysiBb extends CInputWidget {

    const PACKAGE_ID = 'WysiBb-redactor';

    public $package = array();
    public $selector;
    public $options;
    public $show_label = false;

    public function getAssetsPath() {
        return dirname(__FILE__) . '/assets';
    }

    public function getAssetsUrl() {
        return Yii::app()->getAssetManager()->publish($this->getAssetsPath(), true, -1, true);
    }

    function init() {
        if ($this->selector === null) {
            list($this->name, $this->id) = $this->resolveNameID();
            $this->htmlOptions['id'] = $this->getId();
            $this->selector = '#' . $this->getId();

            if ($this->hasModel()) {
                if ($this->show_label) {
                    echo CHtml::activeLabel($this->model, $this->attribute);
                }
                echo CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
            } else {
                echo CHtml::textArea($this->name, $this->value, $this->htmlOptions);
            }
        }
        $this->package = array_merge(array(
            'baseUrl' => $this->getAssetsUrl(),
            'js' => array('jquery.wysibb.js', 'lang/ru.js'),
//	    'js' => array('jquery.wysibb.min.js', 'lang/ru.js'),
            'css' => array('theme/default/wbbtheme.css'),
            'depends' => array('jquery')), $this->package);

        $clientScript = Yii::app()->getClientScript();

        $selector = CJavaScript::encode($this->selector);
        $options = CJavaScript::encode($this->options);

        $clientScript
                ->addPackage(self::PACKAGE_ID, $this->package)
                ->registerPackage(self::PACKAGE_ID)
                ->registerScript(
                        $this->id, 'jQuery(' . $selector . ').wysibb(' . $options . ');', CClientScript::POS_READY
        );
    }

}
