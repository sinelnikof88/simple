<?php

/**
 * A simple PHP BBCode Parser function
 *
 * @author Afsal Rahim
 * @link http://digitcodes.com/create-simple-php-bbcode-parser-function/
 * */
//BBCode Parser function
class WysiBbParser extends CWidget {

    public $text;

    public function init() {
        echo  self::showBBcodes($this->text);
    }

    public static function showBBcodes($text) {
        // BBcode array
        $find = array(
            '~\n~s',
            '~\[table\](.*?)\[\/table\]~s',
            '~\[tr\](.*?)\[\/tr\]~s',
            '~\[td\](.*?)\[\/td\]~s',
            '~\[video\](.*?)\[/video\]~s',
            '~\[sub\](.*?)\[/sub\]~s',
            '~\[sup\](.*?)\[/sup\]~s',
            '~\[s\](.*?)\[/s\]~s',
            '~\[b\](.*?)\[/b\]~s',
            '~\[i\](.*?)\[/i\]~s',
            '~\[u\](.*?)\[/u\]~s',
            '~\[quote\](.*?)\[/quote\]~s',
            '~\[size=(.*?)\](.*?)\[/size\]~s',
            '~\[font=(.*?)\](.*?)\[/font\]~s',
            '~\[color=(.*?)\](.*?)\[/color\]~s',
            '~\[url=(.*?)\](.*?)\[/url]~s', //
            '~\[img width=(.*?)\](.*?)\[\/img\]~s',
            '~\[img\](.*?)\[\/img\]~s',
            '~\[center\](.*?)\[\/center\]~s',
            '~\[right\](.*?)\[\/right\]~s',
            '~\[left\](.*?)\[\/left\]~s',
            '~\[code\](.*?)\[\/code\]~s',
            '~\[\*\](.*?)\[\/\*\]~s',
            '~\[list\](.*?)\[\/list\]~s',
            '~\[list=1\](.*?)\[\/list\]~s',
            '"/[^x\d|*\.]/"',
        );
        // HTML tags to replace BBcode
        $replace = array(
            '<br/>',
            '<table style="width: 100%;">$1</table>',
            '<tr>$1</tr>',
            '<td>$1</td>',
            '<iframe src="http://www.youtube.com/embed/$1" width="640" height="480" frameborder="0"></iframe>',
            '<sub>$1</sub>',
            '<sup>$1</sup>',
            '<strike>$1</strike>',
            '<b>$1</b>',
            '<i>$1</i>',
            '<span style="text-decoration:underline;">$1</span>',
            '<pre>$1</pre>',
            '<font style="line-height: initial;" size="$1">$2</font>',
//	    '<span style="font-size:$1px;">$2</span>',
            '<font face="$1">$2</font>',
            '<font color="$1">$2</font>',
            '<a href=$1>$2</a>', //
            '<img width="$1%" src="$2" alt="" />',
            '<img src="$1" alt="" />',
//	    '<center>$1</center>',
            '<p style="text-align: center;">$1</p>',
            '<p style="text-align: right;">$1</p>',
            '<p style="text-align: left;">$1</p>',
            '<code>$1</code>',
            '<li>$1</li>',
            '<ul>$1</ul>',
            '<ol>$1</ol>',
            ' <wbr>',
        );
     //   var_dump($text);
         $text = str_replace(array("&nbsp;", chr(0xC2).chr(0xA0)), ' <wbr> ', $text);
        return preg_replace($find, $replace, $text);
         // Replacing the BBcodes with corresponding HTML tags
     }

}

?>