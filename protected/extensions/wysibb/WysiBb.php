<?php

/***********<?php
$this->widget('ext.wysibb.WysiBb', array(
    'model' => $model,
    'attribute' => 'text',
));
?>
 * 
 */
class WysiBb extends CInputWidget {

    const PACKAGE_ID = 'WysiBb-redactor';

    public $model, $attribute, $showLabel = true, $connector, $settings = [];
    public $package = array();
    public $selector;
    public $options, $clientScript;
    public $force = true;

    public function getAssetsPath() {
        return dirname(__FILE__) . '/assets';
    }

    public function getAssetsUrl() {
        return Yii::app()->getAssetManager()->publish($this->getAssetsPath(), true, -1, true);
    }

    function init() {


        $this->package = array_merge(array(
            'baseUrl' => $this->getAssetsUrl(),
            'js' => array('jquery.wysibb.js', 'lang/ru.js'),
            'css' => array('theme/default/wbbtheme.css'),
            'depends' => array('jquery')), $this->package);

        $this->clientScript = Yii::app()->getClientScript();
    }

    public function run() {
        list($name, $id) = $this->resolveNameID();
        if (isset($this->htmlOptions['id']))
            $id = $this->htmlOptions['id'];
        else
            $this->htmlOptions['id'] = $id;
        if (isset($this->htmlOptions['name']))
            $name = $this->htmlOptions['name'];

        $id = $this->getId(1);

        $this->render('index', ['showLabel' => $this->showLabel, 'model' => $this->model, 'attribute' => $this->attribute, 'id' => $id, 'name' => $name]);
        $this->registerScripts($id);
    }

    private function registerScripts($id) {
//        echo '<pre>'; print_r($this); exit;
        
        $selector = CJavaScript::encode($this->selector);
        $options = CJavaScript::encode($this->options);

        $this->clientScript
                ->addPackage(self::PACKAGE_ID, $this->package)
                ->registerPackage(self::PACKAGE_ID);
        if ($this->force) {
            $this->clientScript->registerScript($this->id, 'jQuery(' . $id . ').wysibb(' . $options . ');', CClientScript::POS_READY);
        }
    }

    public function register() {
        $this->clientScript
                ->addPackage(self::PACKAGE_ID, $this->package)
                ->registerPackage(self::PACKAGE_ID)
        ;
    }

}
