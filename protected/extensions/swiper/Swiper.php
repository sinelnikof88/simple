<?php

class Swiper extends CWidget {

    public $dataProvider;
    public $type = 'lite';
    public $options = ['grabCursor' => true];
    protected $defaultOptions = [
	'grabCursor' => true,
	'pagination' => '.swiper-pagination',
	'nextButton' => '.swiper-button-next',
	'prevButton' => '.swiper-button-prev',
	'paginationClickable' => true,
	'spaceBetween' => 30,
	'centeredSlides' => true,
	'autoplay' => 2500,
//	 'resizeEvent'=> 'auto',
//	  'slidesPerView'=> '4',
//	  'observer'=> 'true',

	'autoplayDisableOnInteraction' => false
    ];

    public function run() {
	$this->options = array_merge_recursive($this->options, $this->defaultOptions);
	$id = $this->getId();
	$assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets');
	Yii::app()->clientScript->registerCssFile($assets . '/css/swiper.css');
	Yii::app()->clientScript->registerCssFile($assets . '/css/' . $this->type . '.css');
	Yii::app()->clientScript->registerScriptFile($assets . '/js/swiper.js', CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile($assets . '/js/swiper.jquery.js', CClientScript::POS_END);
	$options = !empty($this->options) ? CJavaScript::encode($this->options) : '';
	$this->render('main', ['dataProvider' => $this->dataProvider, 'id' => $id]);
        Yii::app()->clientScript->registerScript('swiper_'.$id, "  Swiper('#{$id}',{$options});", CClientScript::POS_READY);
    }

}
