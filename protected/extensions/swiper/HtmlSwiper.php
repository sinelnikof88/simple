<?php

class HtmlSwiper extends CWidget {

    public $htmlData;
    public $type = 'lite';
    public $options = ['grabCursor' => true];
    protected $defaultOptions = [];
    public $slideNumber=[
        'small'=>1,
        'medium'=>3,
        'big'=>5,
    ];

    public function run() {


        $this->defaultOptions = CJavaScript::jsonDecode("{
      slidesPerView: 3,
       'pagination' : '.swiper-pagination',
	'nextButton' : '.swiper-button-next',
	'prevButton' : '.swiper-button-prev',
      loop: true,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    }");
        $id = $this->getId();
        $this->options = array_replace($this->defaultOptions, $this->options);
        $options = array_replace($this->options, [
//            'slideClass' => 'swiper-slide' . $id,
//            'wrapperClass' => 'swiper-wrapper' . $id
        ]);
        $assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets');
        Yii::app()->clientScript->registerCssFile($assets . '/css/swiper.css');
        Yii::app()->clientScript->registerCssFile($assets . '/css/' . $this->type . '.css');
        Yii::app()->clientScript->registerScriptFile($assets . '/js/swiper.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($assets . '/js/swiper.jquery.js', CClientScript::POS_END);
        $options = !empty($options) ? CJSON::encode($options) : '';
         $this->render('htmlMain', ['data' => $this->htmlData, 'id' => $id]);
        Yii::app()->clientScript->registerScript('swiper_' . $id, " swiper{$id}= Swiper('#{$id}',{$options});", CClientScript::POS_READY);
        Yii::app()->clientScript->registerScript('swiperres_' . $id, ""
               
                . "
 

$(window).load(function(){
 var ww = $(window).width()
  if (ww>1000) swiper{$id}.params.slidesPerView = ".$this->slideNumber['big'].";
  if (ww>468 && ww<=1000) swiper{$id}.params.slidesPerView =".$this->slideNumber['medium']." ;
  if (ww<=468) swiper{$id}.params.slidesPerView = ".$this->slideNumber['small'].";
  swiper{$id}.update() ;
})
$(window).resize(function(){
 var ww = $(window).width()
  if (ww>1000) swiper{$id}.params.slidesPerView = ".$this->slideNumber['big'].";
  if (ww>468 && ww<=1000) swiper{$id}.params.slidesPerView =".$this->slideNumber['medium']." ;
  if (ww<=468) swiper{$id}.params.slidesPerView = ".$this->slideNumber['small'].";
  swiper{$id}.update() ;
})"
                . ""
                . "", CClientScript::POS_READY);
   
        
        
    }

}
