<?php ?>
 
<div id="<?= $id ?>"  class="swiper-container">
    <div class="swiper-wrapper ">
         
       <?php  foreach ($data as $item): ?>
            <div class="swiper-slide  "  > 
                <?= ($item->swiperContent) ?> 
            </div>
        <?php endforeach; ?>
    </div>
    <div class="swiper-pagination"></div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
</div>


 