<?php
$data = ($dataProvider->getData());
?>
 
<div id="<?= $id ?>" class="swiper-container">
    <div class="swiper-wrapper">
        <?php foreach ($data as $item): ?>
            <?php
            if (is_array($item)) {
                $item = (object) $item;
            }
            ?>
            <div class="swiper-slide" style="position: relative"> <?= CHtml::image($item->image) ?>
                <div style="position: fixed;"><?= (isset($item->name) ? $item->name : '') ?></div>
            </div>
        <?php endforeach; ?>
    </div>    
    <div class="swiper-pagination"></div>
    <!-- Add Arrows -->
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
</div>

