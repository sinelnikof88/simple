<?php if ($showLabel): ?>
    <?php echo CHtml::activeLabel($model, $attribute) ?>
<a href="javascript:$('#hid_<?=$id?>').val(''); $('#image_<?=$id?>').attr('style', 'background-image: url()');">очистить</a>
<?php endif; ?>
<div class="example">
    <div class="drop_zone" id ="image_<?= $id ?>" >
        <?php echo CHtml::fileField($name, '', ['id' => $id, 'class' => 'dz-input']) ?> 
        <?php echo CHtml::activeHiddenField($model, $attribute, ['id' => 'hid_' . $id,]) ?>
    </div>
</div>
