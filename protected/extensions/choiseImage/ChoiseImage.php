<?php

class ChoiseImage extends CInputWidget {

    public $model, $attribute, $showLabel = true, $connector, $settings = [];
    private $assetsDir;
    private static $defaultSettings = array();
    public $force = true;

    public function init() {

        $dir = dirname(__FILE__) . '/assets';
        $this->assetsDir = Yii::app()->assetManager->publish($dir, false, -1, YII_DEBUG);
        $this->settings = array_merge(self::$defaultSettings, $this->settings);
    }

    public function run() {
        list($name, $id) = $this->resolveNameID();
        if (isset($this->htmlOptions['id']))
            $id = $this->htmlOptions['id'];
        else
            $this->htmlOptions['id'] = $id;
        if (isset($this->htmlOptions['name']))
            $name = $this->htmlOptions['name'];

        $id = $this->getId(1);
        $this->render('image', ['showLabel' => $this->showLabel, 'model' => $this->model, 'attribute' => $this->attribute, 'id' => $id, 'name' => $name]);
        if ($this->force) {
            $this->registerScripts($id);
        }
    }

    private function registerScripts($id) {
         $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($this->assetsDir . '/choiseImage.js');
        $cs->registerCssFile($this->assetsDir . '/style.css');
        $settings = CJavaScript::encode($this->settings);
        $cs->registerScript("{$id}_ChoiseImage", "ChoiseImage.init('{$id}',{$settings});");
    }

    public function register() {
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($this->assetsDir . '/choiseImage.js');
        $cs->registerCssFile($this->assetsDir . '/style.css');
    }

}
