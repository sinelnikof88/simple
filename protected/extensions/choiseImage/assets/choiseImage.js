var ChoiseImage = function () {
    function init($id, $settings) {
        var $val = $('#hid_' + $id).val();
        getFile($id, $val);

        $(document).on('change', ('#' + $id), function () {
            files = this.files;
            sendfile($id, files, $val);
        });
    }


    function  sendfile($id, files, $val) {
        event.stopPropagation(); // Остановка происходящего

        event.preventDefault();  // Полная остановка происходящего

        var data = new FormData();
        $.each(files, function (key, value) {
            data.append('Images[image]', value);
            data.append('Images[old]', $val);
        });

        // Отправляем запрос

        $.ajax({
//                url: '/submit.php?uploadfiles',
            url: '/Images/Create',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Не обрабатываем файлы (Don't process the files)
            contentType: false, // Так jQuery скажет серверу что это строковой запрос
            success: function (respond, textStatus, jqXHR) {

                // Если все ОК

                if (typeof respond.error === 'undefined') {
                    // Файлы успешно загружены, делаем что нибудь здесь
                    getFile($id, respond.img);
                    // выведем пути к загруженным файлам в блок '.ajax-respond'

                    var files_path = respond.files;
                    var html = '';
                    if(files_path !== null){
                    $.each(files_path, function (key, val) {
                        html += val + '<br>';
                    })}
                    $('.ajax-respond').html(html);
                } else {
                    console.log('ОШИБКИ ОТВЕТА сервера: ' + respond.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ОШИБКИ AJAX запроса: ' + textStatus);
            }

        });
    }
    function getFile($id, file) {
        $('#image_' + $id).css('background-image', 'url(' + file + ')');
        $('#hid_' + $id).val(file);

    }
    return {
        init($id, $settings) {
            init($id, $settings)
        },

    }

}()
 