<?php

class Range extends CInputWidget {

    public $model, $id = false, $attribute, $showLabel = true, $connector, $settings = [];
    private $assetsDir;
    private static $defaultSettings = array();
    public $force = true;
    public  $label;
    public  $decimal = 'false',
            $disable = 'false',
            $disableOpacity = '0.5',
            $hideRange = 'false',
            $klass = '',
            $min = '0',
            $max = '100',
            $start = 'null',
            $step = 'null',
            $vertical = 'false';

    public function init() {

        $dir = dirname(__FILE__) . '/assets';
        $this->assetsDir = Yii::app()->assetManager->publish($dir, false, -1, YII_DEBUG);
        $this->settings = array_merge(self::$defaultSettings, $this->settings);
    }

    public function run() {
        list($name, $id) = $this->resolveNameID();
        if (isset($this->htmlOptions['id']))
            $id = $this->htmlOptions['id'];
        else
            $this->htmlOptions['id'] = $id;
        if (isset($this->htmlOptions['name']))
            $name = $this->htmlOptions['name'];

        $id = ($this->id) ? ($this->id) : "range_" . $this->getId(1);

        $this->render('index', [
            'showLabel' => $this->showLabel,
            'label' => $this->label,
            'model' => $this->model,
            'attribute' => $this->attribute,
            'id' => $id,
            'name' => $name]);
        if ($this->force) {
            $this->registerScripts($id);
        }
    }

    private function registerScripts($id) {
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($this->assetsDir . '/range.js');
        $cs->registerCssFile($this->assetsDir . '/range.css');
        $settings = CJavaScript::encode($this->settings);
        $cs->registerScript("{$id}_range", ""
                . ""
                . " var stp_{$id} = document.querySelector('#{$id}');
                    var initStp_{$id} = new Powerange(stp_{$id}, {
                        decimal:$this->decimal,
                        disable:$this->disable,
                        disableOpacity: $this->disableOpacity,
                        hideRange: $this->hideRange,
                        klass:$this->klass$this->decimal,
                        min:$this->min,
                        max:$this->max,
                        start:$this->start,
                        step:$this->step,
                        vertical: $this->vertical,
                        callback: function (e) {
                            document.getElementById('display-box_{$id}').innerHTML = stp_{$id}.value;
                        }});"
                . "");
    }

}
