
<?php if ($label): ?>
        <?php echo CHtml::label($label,$id) ?>

<?php else: ?>
    <?php if ($showLabel): ?>
        <?php echo CHtml::activeLabel($model, $attribute) ?>
    <?php endif; ?>
<?php endif; ?>



<div class="slider-wrapper" style=" padding: 10px">
    <?php echo CHtml::activeTextField($model, $attribute, ['id' => $id,]) ?>
    <div id="display-box_<?= $id ?>" class="display-box"><? $model->{$attribute} ?></div>
</div>
