<?php

/**
 * TbProgress class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 * @since 0.9.10
 */

/**
 * Bootstrap progress bar widget.
 * @see http://twitter.github.com/bootstrap/components.html#progress
 */
class TbRaiting extends CWidget {

    public $percent = 0;

    /**
     * @var array the HTML attributes for the widget container.
     */
    public $htmlOptions = array();
    public $text        = '';
    public $textPlan    = '';
    public $textFakt    = '';

    /**
     * Initializes the widget.
     */
    public function init() {

        if ($this->percent < 0)
            $this->percent = 0;
        else if ($this->percent > 100)
            $this->percent = 100;
    }

    /**
     * Runs the widget.
     */
    public function run() {

        echo CHtml::openTag('div', array('style' => 'height: 100px;background-color: red; width: 100px; display:inline-block;position: relative;'));
        echo '<div class="bar" style="background-color: rgba(0,0, 255, 1); width: 100%;display: inline-block;position: absolute;bottom: 0px;height: ' . $this->percent . '%;"><span style="color:#fff;display: inline-block;width: 100%;position: absolute;bottom: 0px;width: 100%;text-align: center;">' . (int) $this->percent . ' %<span></div>';
        echo CHtml::closeTag('div');
        echo '<div style="      vertical-align: top;">';
        echo '<div > План: ' . yii::app()->format->formatNumber($this->textPlan) . '</div>';
        echo '<div > Факт: ' . yii::app()->format->formatNumber($this->textFakt) . '</div>';
         echo '</div>';
    }

}
