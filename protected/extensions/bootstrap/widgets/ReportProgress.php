<?php
/**
 * TbProgress class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 * @since 0.9.10
 */

/**
 * Bootstrap progress bar widget.
 * @see http://twitter.github.com/bootstrap/components.html#progress
 */
class ReportProgress extends CWidget
{
    public $color='#FF8B67';

    // Progress bar types.
	const TYPE_INFO = 'info';
	const TYPE_SUCCESS = 'success';
	const TYPE_WARNING = 'warning';
	const TYPE_DANGER = 'danger';

	/**
	 * @var string the bar type. Valid values are 'info', 'success', and 'danger'.
	 */
	public $type;
	/**
	 * @var boolean indicates whether the bar is striped.
	 */
	public $striped = false;
	/**
	 * @var boolean indicates whether the bar is animated.
	 */
	public $animated = false;
	/**
	 * @var integer the amount of progress in percent.
	 */
	public $percent = 0;
	/**
	 * @var array the HTML attributes for the widget container.
	 */
	public $htmlOptions = array();

        public $text='';
        /**
	 * Initializes the widget.
	 */
	public function init()
	{
		$classes = array('progress','report');


		if (!empty($classes))
		{
			$classes = implode(' ', $classes);
			if (isset($this->htmlOptions['class']))
				$this->htmlOptions['class'] .= ' '.$classes;
			else
				$this->htmlOptions['class'] = $classes;
		}

		if ($this->percent < 0)
			$this->percent = 0;
		else if ($this->percent > 100)
			$this->percent = 100;
	}

	/**
	 * Runs the widget.
	 */
	public function run()
	{
            $style = "  background-color: $this->color;
  background-image: -moz-linear-gradient(top,  $this->color,  $this->color);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from( $this->color), to( $this->color));
  background-image: -webkit-linear-gradient(top,  $this->color,  $this->color);
  background-image: -o-linear-gradient(top,  $this->color,  $this->color);
  background-image: linear-gradient(to bottom,  $this->color,  $this->color);";
            
            $id = uniqid('indicator');
		echo CHtml::openTag('div', $this->htmlOptions);
		echo ' <div  class="bar" style="width: '.($this->percent -1) .'%; '.$style.'"></div><a data-parentid ='.$id.' style="
    display: inline-block;
    position: relative;
    left: -8px;" href="#" class ="progressbar" title="'.(int) ($this->percent)  .' %"></a>';
		echo '</div><div style ="'.$style.'" class="popo" id='.$id.' >'.(int) $this->percent.'%</div>';
                
	}
}
