var address_id;
var contragent_id;
var cityName = 'Барнаул';
 
function Mapmarker(coordinates_1, coordinates_2) {
//	console.log('okkey');
    var strCoordinates = ('["' + coordinates_2 + '","' + coordinates_1 + '"]')
    $('#Address_coordinates').val(strCoordinates);

}

function initAddElemetsButtons() {
    $(".add-element-button").click(function () {
        console.log('1');
        elems = $(".element-input", $(this).parent());
        elem = $(elems[0]).clone();
        elem.val('');
        $(this).parent().append(elem);
    });
}

function bildingInfo(elevation) {
//console.log (elevation.result[0].attributes);
    var info = JSON.stringify(elevation.result[0].attributes);
    $('#Waybill_bildingInfo').val(info);
}

function loadComment(id) {

    $.ajax({
        type: "POST", //отправляем методом GET
        url: "/address/load", //скрипту ajax.php
        data: {
            id: id
        },
        success: function (data) {
            var result = JSON.parse(data);

            if (!result.result) {
                $('#Address_comment').html("");
                return;
            }
            $('#Address_comment').html(result.comment);
        }
    });

}

function parseCoordinates(txt) {

    var re1 = '.*?';	// Non-greedy match on filler
    var re2 = '([+-]?\\d*\\.\\d+)(?![-+0-9\\.])';	// Float 1
    var re3 = '.*?';	// Non-greedy match on filler
    var re4 = '([+-]?\\d*\\.\\d+)(?![-+0-9\\.])';	// Float 2
    var p = new RegExp(re1 + re2 + re3 + re4, ["i"]);
    var m = p.exec(txt);
    if (m != null) {
        var float1 = m[1];
        var float2 = m[2];

        return '[' + float2 + ', ' + float1 + ']';
    }
    return;

}

function getTotalId() {
    if (contragent_id == 'undefined') {
        contragent_id = '';
    }
    if (address_id == 'undefined') {
        address_id = '';
    }


    return address_id + ':' + contragent_id;
}


function loadAddressFromDoubleGis(request) {
//console.log('loadAddressFromDoubleGis '+request);
	$.ajax({
        type: "GET", //отправляем методом GET
        url: "http://catalog.api.2gis.ru/geo/search?callback=?",
        dataType: "json",
        data: {
            q: request,
            format: 'full',
            output: "jsonp",
            version: "1.3",
            key: "ruxyqs1897",
        },
        success: function (name) {

            address_id = name.result[0].id;
            loadComment(getTotalId()); //??
            console.log('org' + address_id);
            getTotalId(name.result[0].id);
            bildingInfo(name);
            $("#coordinates").val(parseCoordinates(name.result[0].centroid));

            $("#inform").html('<div id="addressvalue" >' + name.result[0].name + '</div>');
            $("#addressvalue").on('click', function () {
                bildingInfo(name);
                $("#Address_address_text").val($(this).html().replace(cityName + ", ", ""));
//				console.log($(this).html());

            });

            $(".name_organisation").on('click', '', function () {
                var name = $(this).html()

            });
            var object = name.result;

            var dataid = name.result[0].id;
            //		console.log(name.result[0]);


            $.ajax({
                type: "GET", //отправляем методом GET
                url: "http://catalog.api.2gis.ru/geo/get?callback=?",
                dataType: "json",
                data: {id: dataid, output: "jsonp", version: "1.3", key: "ruxyqs1897", },
                success: function (data) {
                    $("#Address_address_text").attr({class: "sucsess_input"});
                    //		$('#Address_double_gis_id').popover('show')
                    //				console.info(data.result[0].centroid);

                    var txt = data.result[0].centroid;
                    var re1 = '.*?';	// Non-greedy match on filler
                    var re2 = '(POINT)';	// Word 1
                    var re3 = '.*?';	// Non-greedy match on filler
                    var re4 = '([+-]?\\d*\\.\\d+)(?![-+0-9\\.])';	// Float 1
                    var re5 = '.*?';	// Non-greedy match on filler
                    var re6 = '([+-]?\\d*\\.\\d+)(?![-+0-9\\.])';	// Float 2

                    var p = new RegExp(re1 + re2 + re3 + re4 + re5 + re6, ["i"]);
                    var m = p.exec(txt);
                    if (m != null) {
                        var float1 = m[2];
                        var float2 = parseFloat(m[3])+0.00005; 	//: рисовать маркер чуть выше адреса. Когда поиск гео объекта был по адресу
                    }											//: координаты маркера (centroid) и адреса не совпадали.

                    HMap.myMap.clearMap();
                    HMap.myMap.addMarker([float1, float2], data.result[0].name);
                    Mapmarker(float2, float1);

                }
            });

        }
    });
}


function initSearchAddress() {
    $("#Address_address_text").bind('textchange', function () {
        loadAddressFromDoubleGis($(this).val() + " " + cityName);
    });
}


function loadOrganizationFromDoubleGis(search) {
    $.ajax({
        url: "http://catalog.api.2gis.ru/search?callback=?",
        dataType: "json",
        data: {
            output: "jsonp",
            what: search,
            where: cityName,
            version: "1.3",
            key: "ruxyqs1897",
        },
        success: function (object) {
            $("#inform").html("");
            HMap.myMap.clearMap();
            //console.log(object.result[0].id); // parse organisation id
            //getOrganisationId(object.result[0].id);
            contragent_id = object.result[0].id;
            console.log('org' + contragent_id);
            loadComment(getTotalId()); //??

            $(object.result).each(function (index, item) {

                var arr = [item.name];
                var arr2 = [item.id];
                var arr3 = [item.city_name + ', ' + item.address];
                //console.log(item.id + ' - ' + item.city_name + ', ' + item.address);

                HMap.myMap.addMarker([item.lon, item.lat], item.name);
//HMap.myMap.addMarker(arr3, arr);
                for (var i = 0; i < arr.length; i++) {
                    $("#inform").append("<div class='selector' org=" + arr[i] + " val='" + arr3[i] + "' id='" + arr2[i] + "' ><div class='name_organisation'>" + arr[i] + "</div><u>" + arr3[i] + "</u></div><br>");
                }
            });


        },
        beforeSend: function () {
            $('#inform').html('<center><img src="/assets/e3ec aab1/img/preloader.gif" width="70" height="70" border="0" alt="loader"/></center>');
        },
        failture: function (object) {
            alert("2");
        },
        scope: this



    });
}

function loadContragentDataFromDoubleGis(id) {
    var contragentData;

    $.ajax({
        url: "http://catalog.api.2gis.ru/profile?callback=?",
        dataType: "json",
        data: {output: "jsonp", id: id, version: "1.3", key: "ruxyqs1897", },
        success: function (object) {
            //console.log(object);// профиль организации
            contragentData = JSON.stringify(object);

            //loadComment(object.id);
            if (typeof object.id === "string") {
                Mapmarker(object.lat, object.lon); // добавление в доп поле
                HMap.myMap.clearMap();
                HMap.myMap.addMarker([object.lon, object.lat], object.name); // ставит точку

                $('#data_inform').val(contragentData);
            }
            loadAddressFromDoubleGis(object.lon + ',' + object.lat); //: object.city_name + ', ' + object.address
        }
    });
    return contragentData;
}

function initSearchOrganization() {

    $("#Contragent_name").bind('textchange', function () {
        //$('#Contragent_name').popover('show')
        loadOrganizationFromDoubleGis($("#Contragent_name").val());
    });

    $("#inform").on('click', '.selector', function () {


        $('#Contragent_name').popover('hide');
        var id = $(this).attr("id");
        $('#gisId').val(id);

        var val = $(this).attr("val");
        var name = $('.name_organisation', $(this)).html();

        $("#Contragent_name").val(name).attr({class: "sucsess_input"});
        $("#Address_address_text").val(val);

        loadContragentDataFromDoubleGis(id);

        //	console.log(id);


    });
}

function initSubmit() {
    $("#horizontalForm").bind('submit', function () {
        //Address_double_gis_id
        $('#Address_double_gis_id').val(getTotalId());
//alert(getTotalId());
        //  return false;
    });
}
$(document).ready(function () {
//$('#Contragent_name').popover('show')
//$('#Address_address_text').popover('show')
    initSearchAddress();
    initAddElemetsButtons();
    initSearchOrganization();
    initSubmit();

});