function submitForSelect(){
    $(this).bind('keyup', function (e) {
        e = e || window.event;
        if (e.keyCode === 13) {
            $('form.form-inline').submit();
        }
        return false;
    });
}