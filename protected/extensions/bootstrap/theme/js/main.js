function init() {

    $('.del').click(function (e) {
	if (confirm('Уверен?')) {
	    $.get($(this).attr('href'));
	    ($(this).parent().parent().remove());

	    $.get('/analitica/workarea/getPlan/id/' + $(this).data('manager_id'), function (datap) {
		$('#userData' + $(this).data('manager_id')).html(datap);
	    })
	}
	e.preventDefault();
    })
    $('.rowChekedButton').click(function (e) {
//	if (confirm('Уверен?')) {
	$this = this;
	$.getJSON($(this).attr('href'), [], function (data) {
	    if (data.tag == true) {
		$($this).parent().parent().addClass('rowCheked')
	    } else {
		$($this).parent().parent().removeClass('rowCheked')
	    }
	});

//	}
    })

    $('.editable2').click(function () {
	$.each($('.editable2'), function (i, index) {
	    $(index).removeClass('edithis')
	})
	$('#editDiv').show()
	$(this).addClass('edithis');
	$('#editDiv').css('display', 'inline-block')
	//отчищаем форму
	document.getElementById("WorkareaForm-form").reset();
	//загружаем новые данные
	$.each($(this).data(), function (i, index) {
	    $('#Data_' + i).val(index)
	})
	$('#editDiv').offset({top: $(this).offset().top, left: $(this).offset().left})
    })

    $('#WorkareaForm-form').on('submit', function (e) {
	e.preventDefault();
	$.ajax({
	    type: "POST",
	    dataType: 'json',
	    url: $("#WorkareaForm-form").attr('action'),
	    data: $("#WorkareaForm-form").serialize(), // serializes the form's elements.
	    success: function (data)
	    {
		if (data == 'reload') {
		    //перезагружаем страницу
		    $(location).attr('href', $(location).attr('href'));
		}
		$().toastmessage('showToast', {
		    text: 'Данные изменены',
		});
		$('.edithis').html(data); // show response from the php script.
		/**/
		$.each(data, function (i, index) {
		    $('#Data_' + i).val(index)
		    $('.edithis').data(i, index);
		})
		$('.edithis').html(data.value);

		$('#editDiv').hide();

		$.get('/analitica/workarea/getPlan/id/' + data.user_id, function (datap) {
		    $('#userData' + data.user_id).html(datap);
		})

	    }

	});

    })
// скрываем панельку при переключений табов
    $(document).on('click', "a[data-toggle='tab']", function () {
	$('#editDiv').hide();

    })
//    $("#grid").tablesorter();
}
//setInterval(function () {
//    $.get('/messages/new', function (data) {
//	console.log(parseInt(data));
//	if (parseInt(data) > 0) {
//	    $('.topMessage').html("<span style='color:red'>Eсть новые (" + data + ')</span>')
//	   
//	}
//    });
//}, 5000)



function getTimeRemaining(endtime, p) {
    var t = Date.parse(endtime) - Date.parse(new Date().addHours(p));
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
	'total': t,
	'days': days,
	'hours': hours,
	'minutes': minutes,
	'seconds': seconds
    };
}

function initializeClock(id, endtime, p) {
    var clock = document.getElementById(id);
    var timeinterval = setInterval(function () {
	var t = getTimeRemaining(endtime, p);
	clock.innerHTML = ('0' + t.hours).slice(-2) + '' + ':' + ('0' + t.minutes).slice(-2) + '' + ':' + ('0' + t.seconds).slice(-2);
	if (t.total <= 0) {
	    clearInterval(timeinterval);
	}
    }, 1000);
}
Date.prototype.addHours = function (h) {
    this.setHours(this.getHours() + h);
    return this;
};

$(document).on('click', '#controll', function () {
    if (!$(this).hasClass('active')) {
        $(this).addClass('active')
	$(".editable2.tt").each(function (i, index) {
	    $(index).addClass('viewOld')
	    $(index).html($(index).data('title'))
	});
    } else {
	$(".editable2.tt").each(function (i, index) {
	    $(index).removeClass('viewOld')
	    $(index).html($(index).data('value'))
	});
        $(this).removeClass('active')
    }
});


jQuery(document).ready(function($){
    if ($('.popoverhover').length) {
        $('.popoverhover').popover({
            html: true,
            trigger: 'hover',
            content: function(){
                return $(this).siblings('.popoverhover_content').html();
            }
        });
    }
});
