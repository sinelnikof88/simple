<?php

class ContentController extends Controller {

    public $layout = '/layouts/column2';

    public function actionView($name = false) {
        $page = Pages::model()->by('slug', $name)->find();
        $this->render('view', array(
            'model' => $page,
        ));
    }

}
