<?php

class ImagesController extends Controller {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'load', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update','create'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.

      public function actionCreate() {
      $model = new Images;

      // Uncomment the following line if AJAX validation is needed
      // $this->performAjaxValidation($model);

      if (isset($_POST['Images'])) {
      $model->attributes = $_POST['Images'];
      if ($model->save()) {
      $this->redirect(array('view', 'id' => $model->id));
      } else {
      print_r($model->getErrors());
      }
      }

      $this->render('create', array(
      'model' => $model,
      ));
      }
     */
    public function actionCreate() {
        $model = new Images;
        if (isset($_POST['Images']['old'])) {
            $old = $_POST['Images']['old'];
            $oldModel = Images::model()->by('path', $old)->find();
            if ($oldModel) {
                try {
                    $oldModel->delete();
                } catch (Exception $exc) {
//                echo $exc->getTraceAsString();
                }
            }
        }
        if (isset($_FILES['Images'])) {
            $model->attributes = $_FILES['Images'];

            $model->image = CUploadedFile::getInstance($model, 'image');
            $model->name = $model->image->getName();
            $model->ext = $model->image->getExtensionName();

            $patch = '/images/' . yii::app()->user->id . '/' . date('Y-m-d/H_i_s');
            $model->path = $patch . '/' . time() . '.' . $model->ext;
            if (!is_dir(Yii::getPathOfAlias('webroot') . $patch)) {
                mkdir(Yii::getPathOfAlias('webroot') . $patch, 0777, true);
            }
            if ($model->save()) {
                $path = Yii::getPathOfAlias('webroot') . $model->path;
                $model->image->saveAs($path);
                echo json_encode(['img' => $model->path]);
                exit;
                // перенаправляем на страницу, где выводим сообщение об
                // успешной загрузке
            } else {
                print_r($model->getErrors());
                exit;
            }
        }
        $this->render('create', array('model' => $model));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Images'])) {
            $model->attributes = $_POST['Images'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Images('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Images']))
            $model->attributes = $_GET['Images'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Images::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'images-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
