<div class="container">
    <div class="main-page-content row">
        <!--<div class="content-page col-lg-8 col-md-8 col-sm-12 col-xs-12">-->
        <div class="content-page col-lg-8 col-md-8 col-sm-12 col-xs-12">

            <div id="wp-content" class="wp-content clearfix">
                <article id="post-257" class="post-257 post type-post status-publish format-gallery has-post-thumbnail hentry category-fashion post_format-post-format-gallery">
                    <div class="entry-content">
                        <div class="content-inner">
                            <h1 class="entry-title"><?= $model->name ?></h1>
                            <div class="field-item even" style="min-height: 300px">
                                <?php
                                $this->widget('ext.wysibb.WysiBbParser', array(
                                    'text' => $model->text,
                                ));
                                ?>
                            </div>
                        </div>
                </article>

            </div>

            <div class="vc_row wpb_row vc_row-fluid remove_padding_top row-container">
                <div class="container">
                    <div class="row ">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <? $this->widget('ext.calculator.CalculatorWiget'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--<div class="column-border wpb_column vc_column_container vc_col-sm-4 vc_col-lg-4 vc_col-md-4">-->

        <div class="sidebar wp-sidebar sidebar-right column-border wpb_column vc_column_container  col-lg-4 col-md-4 col-xs-12 pull-right">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="widget gsc-heading  align-left style-2 text-dark">
                        <h2 class="title">
                            <span>Заявка на займ </span>
                        </h2>  
                        <div class="line">
                            <span>
                            </span>
                        </div>
                        <div class="title-desc">Заполните и отправьте форму, и мы свяжемся с вами в течение 5 минут.</div>
                    </div>
                    <?php $this->renderPartial('//site/_form') ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
//http://black-town.ru/themes/business/js/jquery.themepunch.tools.min.js?ver=5.4.8
//$this->widget('bootstrap.widgets.TbDetailView', array(
//    'data' => $model,
//    'attributes' => array(
//        'id',
//        'name',
//        'title',
//        'description',
//        'text',
//        'is_active',
//        'image',
//    ),
//));
?>
