<?php

$model = Pages::model()->by('group_id', 1)->order('id desc')->findAll();
foreach ($model as $data) {
    $data->swiperContent = $this->renderPartial('_pagesItems', ['data' => $data], true, false);
}

$this->widget('ext.swiper.HtmlSwiper', array(
    'htmlData' => $model,
    'options' => [
    ]
));
?>