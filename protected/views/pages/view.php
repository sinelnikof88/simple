<?php
$this->breadcrumbs = array(
    'Pages' => array('index'),
    $model->name,
);
?>

<h1>View Pages #<?php echo $model->id; ?></h1>

<?php
$this->widget('ext.wysibb.WysiBbParser', array(
    'text' => $model->text,
));
?>
